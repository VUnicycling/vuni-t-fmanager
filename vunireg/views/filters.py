import django_filters
from django import forms
from vunireg.models import Person, Club, Entry, Competition

class PersonFilter(django_filters.FilterSet):

    class Meta:
        model = Person
        fields = {'forename': ['icontains',],
                  'surname': ['icontains',],
                  'gender': ['exact',],
                  'dateofbirth': ['gt',],
                  'club': ['exact',],
                 }
            

class ClubFilter(django_filters.FilterSet):

    class Meta:
        model = Club
        fields = '__all__'
        fields = {'clubname': ['icontains',],
                  'individualstart': ['exact',],
                 }


class EntryFilter(django_filters.FilterSet):
    discipline = django_filters.CharFilter(field_name='discipline__name')
    competition = django_filters.CharFilter(field_name='competition__fullname')
    forename = django_filters.CharFilter(field_name='person__forename')
    surname = django_filters.CharFilter(field_name='person__surname')
    gender = django_filters.CharFilter(field_name='person__gender')
    club = django_filters.CharFilter(field_name='person__club__clubname')

    class Meta:
        model = Entry
        fields = '__all__'
        fields = {'discipline': ['exact',],
                  'competition': ['exact',],
                  'forename': ['icontains',],
                  'surname': ['icontains',],
                  'gender': ['exact',],
                  'club': ['icontains',],
                 }
             
                 
class CompetitionFilter(django_filters.FilterSet):
#    maxage__gt = django_filters.NumberFilter(field_name='maxage', lookup_expr='gt')
#    maxage__lt = django_filters.NumberFilter(field_name='maxage', lookup_expr='lt')

    class Meta:
        model = Competition
        fields = '__all__'
        fields = {'discipline': ['exact',],
                  'fullname': ['icontains',],
                  'maxage': ['gt','lt',],
                  'gender': ['exact',],
                  'entrycount': ['gt',],
                 }