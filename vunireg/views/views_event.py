from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
import datetime
import csv
import types
from django.db.models import F, Func

from django.template.loader import render_to_string
from django.conf import settings
#from weasyprint import HTML, CSS
import tempfile

# Create your views here.
from vunireg.models import Event, Person, Club, Entry, Discipline, Agegroup, Competition, Start, Scoringcategory, Qualification, Result
from .filters import PersonFilter, ClubFilter, EntryFilter, CompetitionFilter
from .forms import EventEditForm, PersonEditForm, EntryEditInlineForm, ScoringcatEditForm, QualificationEditForm, ResultEditInlineFormset


def test(request):
    return render(request, 'base.html')

def home(request):    
    return render(request, 'vunireg/home.html')

###
### Wettkampf unabhängige Funktionen zur Verwaltung mehrerer Wettkämpfe
###

# Funktion, die die Übersichtsseite zum Auswählen eines Wettkampfes zurückgibt
def index(request):
    # Ausgabe
    content = {
        "event_list": Event.objects.all(),
    }
    return render(request, 'vunireg/home_event_list.html', content)

# Funktion, die die Seite zum Hinzufügen eines neuen Wettkampfes zurückgibt
def event_add(request):
    response = ''
    error = ''
    if request.method == 'POST':
        form = EventEditForm(request.POST)
        if form.has_changed():
            if form.is_valid():
                form.save()
                # Anlegen der Standardwertungen
                event = get_object_or_404(Event, slug=form.instance.slug)
                scoringcat = Scoringcategory.objects.create(event=event, fullname=event.slug + ' AK', description='Altersklassenwertung ' + event.name, type='COM')
                scoringcat.save()
                scoringcat = Scoringcategory.objects.create(event=event, fullname=event.slug + ' Finale', description='Finalwertung ' + event.name, type='FIN', minage=15, maxage=100)
                scoringcat.save()
                scoringcat = Scoringcategory.objects.create(event=event, fullname=event.slug + ' Juniorfinale', description='Juniorfinalwertung ' + event.name, type='FIN', minage=0, maxage=14)
                scoringcat.save()
                response += 'Die Änderungen wurden erfolgreich gespeichert!'
            else:
                error += 'Das Formular konnte nicht validiert werden!'
    else:
        form = EventEditForm()
    # Ausgabe
    content = {
        'form': form, 'error':error, 'response':response,
    }
    return render(request, 'vunireg/home_event_add.html', content)


###
### Funktionen zur Verwaltung der Basisdaten eines Wettkampfes
###

# Funktion, die die Übersichtsseite für den Bereich Wettkampf zurückgibt
def event(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    competition_list = Competition.objects.filter(event__id=event.pk, round=1)
    final_list = Competition.objects.filter(event__id=event.pk, round=2)
    discipline_list = Discipline.objects.filter(competition__in=competition_list).distinct()
    scoringcat_list = Scoringcategory.objects.filter(event__id=event.pk)
    qualification_list = Qualification.objects.filter(event__id=event.pk)
    
    qualification_table = []
    # Erste Reihe: 'Alter' / Disziplin 1 / Disziplin 2 / ...
    row = [ 'Alter' ]
    disciplines = Discipline.objects.filter(qualification__in=qualification_list).distinct()
    for discipline in disciplines:
        row.append(discipline.name)
    qualification_table.append(row)
    # Weitere Reihen Reihe: Alter von ... bis ... / Quali Disziplin 1 / Quali Disziplin 2 / ...
    agetuples = (Qualification.objects.values_list('minage', 'maxage').order_by('minage').distinct())
    for agetuple in agetuples:
        if (agetuple[1] < 99):
            row = [ str(agetuple[0]) + ' - ' + str(agetuple[1]) + ' Jahre']
        else:
             row = [ str(agetuple[0]) + ' Jahre und älter']
        for discipline in disciplines:
            try:
                tempqualification = qualification_list.get(discipline__id=discipline.pk, minage=agetuple[0], maxage=agetuple[1])
                row.append(tempqualification.value)
            except Qualification.DoesNotExist:
                row.append('')
        qualification_table.append(row)    
    # Ausgabe
    content = {
        'discipline_list':discipline_list,  'competition_list':competition_list, 'final_list':final_list, 'scoringcat_list':scoringcat_list, 'qualification_table':qualification_table, 'event':event, 'error':error, 'response':response,
    }
    return render(request, 'vunireg/event_overview.html', content)


# Funktion, die die Seite zum Bearbeiten der Wettkampfdaten zurückgibt
def event_edit_eventdata(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    form = EventEditForm(request.POST or None, instance=event)
    response = ''
    error = ''
    # Auswerten der Benutzereingaben
    if request.method == "POST":
        if form.has_changed():
            if form.is_valid():
                form.save()
                response += 'Die Änderungen wurden erfolgreich gespeichert!'
            else:
                error += 'Das Formular konnte nicht validiert werden!'
    else:
        form = EventEditForm(instance=event)
    # Ausgabe
    content = {
        'form': form, 'event':event, 'error':error, 'response':response,
    }
    return render(request, 'vunireg/event_edit_eventdata.html', content)

# Funktion, die die Seite zum Bearbeiten der Wertungskategorien zurückgibt
### TODO ###
### Filterwert "Verein" -> Eingabe des Namens und nicht der club_id (Auswahlliste!?)
### Überarbeiten der is_valid() Funktion um in der Datenbank die club_id zu speichern
### ENDTODO ###
def event_edit_scoringcat(request, event_id, scoringcat_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    scoringcat_list = Scoringcategory.objects.filter(event__id=event.pk)
    # Auswerten der Benutzereingaben
    if request.method == 'POST':
        if not scoringcat_id == 'None':
            scoringcat = get_object_or_404(Scoringcategory, pk=scoringcat_id)
            form = ScoringcatEditForm(request.POST, instance=scoringcat)
            if form.has_changed():
                if form.is_valid():
                    newscoringcat = form.save(commit=False)
                    newscoringcat.event = event
                    if newscoringcat.filtertype1 == newscoringcat.filtertype2 and newscoringcat.filtertype1 != None:
                        error += 'Es kann nicht zwei mal der gleiche Filtertype ausggewählt werden!'
                    elif newscoringcat.filtertype1 == 'club':
                        print ('Filtertype1: ' + newscoringcat.filtertype1)
                        if not newscoringcat.filtervalue1 == None:
                            print ('Filtervalue1: ' + newscoringcat.filtervalue1)
                            try:
                                club = Club.objects.get(clubname__iexact=newscoringcat.filtervalue1)
                                newscoringcat.filtervalue1 = club.pk
                                newscoringcat.save()
                                print ('Filtervalue1: ' + str(newscoringcat.filtervalue1))
                                response += 'Die Änderungen wurden erfolgreich gespeichert!'
                            except Club.DoesNotExist:
                                try:
                                    club = Club.objects.get(pk__iexact=newscoringcat.filtervalue1)
                                    newscoringcat.filtervalue1 = club.pk
                                    newscoringcat.save()
                                    print ('Filtervalue1: ' + str(newscoringcat.filtervalue1))
                                    response += 'Die Änderungen wurden erfolgreich gespeichert!'
                                except Club.DoesNotExist:
                                    error += 'Es kann kein Verein zu der Eingabe gefunden werden!'
                        else:
                            newscoringcat.save()
                            response += 'Die Änderungen wurden erfolgreich gespeichert!'
                    elif newscoringcat.filtertype2 == 'club':
                        print ('Filtertype2: ' + newscoringcat.filtertype2)
                        if not newscoringcat.filtervalue2 == None:
                            print ('Filtervalue2: ' + newscoringcat.filtervalue2)
                            try:
                                club = Club.objects.get(clubname__iexact=newscoringcat.filtervalue1)
                                newscoringcat.filtervalue2 = club.pk
                                newscoringcat.save()
                                print ('Filtervalue2: ' + str(newscoringcat.filtervalue2))
                                response += 'Die Änderungen wurden erfolgreich gespeichert!'
                            except Club.DoesNotExist:
                                try:
                                    club = Club.objects.get(pk__iexact=newscoringcat.filtervalue2)
                                    newscoringcat.filtervalue2 = club.pk
                                    newscoringcat.save()
                                    print ('Filtervalue2: ' + str(newscoringcat.filtervalue2))
                                    response += 'Die Änderungen wurden erfolgreich gespeichert!'
                                except Club.DoesNotExist:
                                    error += 'Es kann kein Verein zu der Eingabe gefunden werden!'
                        else:
                            newscoringcat.save()
                            response += 'Die Änderungen wurden erfolgreich gespeichert!'
                    else:
                        newscoringcat.save()
                        response += 'Die Änderungen wurden erfolgreich gespeichert!'
                else:
                    error += 'Das Formular konnte nicht validiert werden!'
            else:
                response += 'Keine Änderungen vorgenommen!'
        else:
            form = ScoringcatEditForm(request.POST)
            if form.has_changed():
                if form.is_valid():
                    newscoringcat = form.save(commit=False)
                    newscoringcat.event = event
                    newscoringcat.save()
                    response += 'Die Wertungskategorie wurde erfolgreich angelegt!'
                else:
                    error += 'Das Formular konnte nicht validiert werden!'
    elif not scoringcat_id == 'None':
        scoringcat = get_object_or_404(Scoringcategory, pk=scoringcat_id)
        form = ScoringcatEditForm(instance=scoringcat)
    else:
        form = ScoringcatEditForm()
    # Ausgabe
    content = {
        'scoringcat_list':scoringcat_list, 'scoringcat_id':scoringcat_id, 'form':form, 'event':event, 'error':error, 'response':response,
    }
    return render(request, 'vunireg/event_edit_scoringcat.html', content)
    
# Funktion, die die Seite zum Bearbeiten der Wettbewerbe zurückgibt
def event_edit_competition(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    # Bereits vorhandene Wettbewerbe ermitteln
    competition_list = Competition.objects.filter(event__id = event.pk, round = 1)
    final_list = Competition.objects.filter(event__id = event.pk, round = 2)
    discipline_list = Discipline.objects.filter(competition__in = competition_list).distinct()
    ### TODO ###
    ### Hier könnte ggf. noch eine Filterung erfolgen, damit das ganze nicht zu unübersichtlich wird und nur bestimmte Disziplinen und Altersklassen angezeigt werden
    disciplinequery = Discipline.objects.filter(group=event.group)
    agegroupquery = Agegroup.objects.all().order_by('maxage')
    ### ENDTODO ###  
    finals = types.SimpleNamespace()
    # Nur bei vorhandener Finalwertung (finals.count > 0) können entsprechende Final-Wettbewerbe erzeugt werden
    final_categories = Scoringcategory.objects.filter(event=event, type='FIN')
    finals.count = final_categories.count()
    ### TODO ###
    ### Berücksichtigen der Unterschiedlichen Finalwertungen (v.a. hinsichtlich Altersaufteilung)
    ### Implementierung, dass nur für Disziplinen Final-Wettbewerbe erzeugt werden können, für die es auch AK-Wettbewerbe gibt
    ### ENDTODO ###
    response = ''
    error = ''
    # Auswerten der Benutzereingaben
    if request.method == "POST":
        disciplines = request.POST.getlist('discipline')
        agegroups = request.POST.getlist('agegroup')
        final_disciplines = request.POST.getlist('final-discipline')
        # Anlegen der Altersklassen-Wettbewerbe
        for discipline in disciplines:
            # Prüfen, ob Disziplin vorhanden
            try:
                discipline_toadd = Discipline.objects.get(code = discipline)
            except Discipline.DoesNotExist:
                error += 'ERROR: Disziplin ' + discipline + ' nicht vorhanden!' + '</br>'
                break
            for agegroup in agegroups:
                agegroup_toadd = Agegroup.objects.get(code = agegroup)
                # Prüfen, ob Wettbewerb bereits vorhanden und ggf. neu anlegen
                # Bei Mixed Disziplinen wir ein gemeinsamer Wettbewerb männlich/weiblich erzeugt
                if discipline_toadd.mixedgender == True:
                    c = Competition.objects.filter(discipline = discipline_toadd, round = 1, fullname = agegroup_toadd.name, event = event)
                    try:
                        c[0]
                        error += 'HINWEIS: Der Wettbewerb ' + discipline_toadd.name + ' - ' + agegroup_toadd.name + ' ist bereits vorhanden!' + '</br>'
                    except IndexError:
                        c = Competition(round = 1, fullname = agegroup_toadd.name, discipline = discipline_toadd, maxage = agegroup_toadd.maxage, gender = '', ismerged = False, event = event)
                        c.save()
                        response += 'HINWEIS: Der folgende Wettbewerb wurde neu angelegt:'
                        response += c.discipline.name + ' ' + c.fullname + ' - ' + str(c.maxage) + ' - ' + c.gender + ' - ' +  str(c.ismerged) + ' - ' + c.event.name + '</br>'
                # Bei allen anderen Disziplinen wir ein jeweils ein Wettbewerb für männlich/weiblich erzeugt
                else:
                    c_male = Competition.objects.filter(discipline = discipline_toadd, round = 1, fullname = 'M' + agegroup_toadd.name, event = event)
                    c_female = Competition.objects.filter(discipline = discipline_toadd, round = 1, fullname = 'W' + agegroup_toadd.name, event = event)
                    try:
                        c_male[0]
                        error += 'HINWEIS: Der Wettbewerb ' + discipline_toadd.name + ' - M' + agegroup_toadd.name + ' ist bereits vorhanden!' + '</br>'
                    except IndexError:
                        c = Competition(round = 1, fullname = 'M' + agegroup_toadd.name, discipline = discipline_toadd, maxage = agegroup_toadd.maxage, gender = 'm', ismerged = False, event = event)
                        c.save()
                        response += 'HINWEIS: Der folgende Wettbewerb wurde neu angelegt:'
                        response += c.discipline.name + ' ' + c.fullname + ' - ' + str(c.maxage) + ' - ' + c.gender + ' - ' +  str(c.ismerged) + ' - ' + c.event.name + '</br>'
                    try:
                        c_female[0]
                        error += 'HINWEIS: Der Wettbewerb ' + discipline_toadd.name + ' - W' + agegroup_toadd.name + ' ist bereits vorhanden!' + '</br>'
                    except IndexError:
                        c = Competition(round = 1, fullname = 'W' + agegroup_toadd.name, discipline = discipline_toadd, maxage = agegroup_toadd.maxage, gender = 'f', ismerged = False, event = event)
                        c.save()
                        response += 'HINWEIS: Der folgende Wettbewerb wurde neu angelegt:'
                        response += c.discipline.name + ' ' + c.fullname + ' - ' + str(c.maxage) + ' - ' + c.gender + ' - ' +  str(c.ismerged) + ' - ' + c.event.name + '</br>'
        # Anlegen der Final-Wettbewerbe
        for discipline in final_disciplines:
            # Prüfen, ob Disziplin vorhanden
            try:
                discipline_toadd = Discipline.objects.get(code = discipline)
            except Discipline.DoesNotExist:
                error += 'ERROR: Disziplin ' + discipline + ' nicht vorhanden!' + '</br>'
                break
            for final in final_categories:
                final_toadd = final
                # Prüfen, ob Wettbewerb bereits vorhanden und ggf. neu anlegen
                # Bei Mixed Disziplinen wir ein gemeinsames Finale männlich/weiblich erzeugt
                if discipline_toadd.mixedgender == True:
                    c = Competition.objects.filter(discipline = discipline_toadd, round = 2, fullname = final_toadd.shortdescription, event = event)
                    try:
                        c[0]
                        error += 'HINWEIS: Der Wettbewerb ' + discipline_toadd.name + ' - ' + final_toadd.shortdescription + ' ist bereits vorhanden!' + '</br>'
                    except IndexError:
                        c = Competition(round = 2, fullname = final_toadd.shortdescription, discipline = discipline_toadd, maxage = final_toadd.maxage, gender = '', ismerged = False, event = event)
                        c.save()
                        response += 'HINWEIS: Der folgende Wettbewerb wurde neu angelegt:'
                        response += c.discipline.name + ' ' + c.fullname + ' - ' + str(c.maxage) + ' - ' + c.gender + ' - ' +  str(c.ismerged) + ' - ' + c.event.name + '</br>'
                # Bei allen anderen Disziplinen wir ein jeweils ein Wettbewerb für männlich/weiblich erzeugt
                else:
                    c_male = Competition.objects.filter(discipline = discipline_toadd, round = 2, fullname = final_toadd.shortdescription + 'männlich', event = event)
                    c_female = Competition.objects.filter(discipline = discipline_toadd, round = 2, fullname = final_toadd.shortdescription + 'weiblich', event = event)
                    try:
                        c_male[0]
                        error += 'HINWEIS: Der Wettbewerb ' + discipline_toadd.name + ' - ' + final_toadd.shortdescription + ' männlich' + ' ist bereits vorhanden!' + '</br>'
                    except IndexError:
                        c = Competition(round = 2, fullname = final_toadd.shortdescription + ' männlich', discipline = discipline_toadd, maxage = final_toadd.maxage, gender = 'm', ismerged = False, event = event)
                        c.save()
                        response += 'HINWEIS: Der folgende Wettbewerb wurde neu angelegt:'
                        response += c.discipline.name + ' ' + c.fullname + ' - ' + str(c.maxage) + ' - ' + c.gender + ' - ' +  str(c.ismerged) + ' - ' + c.event.name + '</br>'
                    try:
                        c_female[0]
                        error += 'HINWEIS: Der Wettbewerb ' + discipline_toadd.name + ' - ' + final_toadd.shortdescription + ' weiblich' + ' ist bereits vorhanden!' + '</br>'
                    except IndexError:
                        c = Competition(round = 2, fullname = final_toadd.shortdescription + ' weiblich', discipline = discipline_toadd, maxage = final_toadd.maxage, gender = 'f', ismerged = False, event = event)
                        c.save()
                        response += 'HINWEIS: Der folgende Wettbewerb wurde neu angelegt:'
                        response += c.discipline.name + ' ' + c.fullname + ' - ' + str(c.maxage) + ' - ' + c.gender + ' - ' +  str(c.ismerged) + ' - ' + c.event.name + '</br>'
    # Ausgabe
    content = {
        'discipline_list':discipline_list,  'competition_list':competition_list, 'final_list':final_list, "disciplinequery":disciplinequery, "agegroupquery":agegroupquery, 'finals':finals, 'event':event, 'error':error, 'response':response,
    }
    return render(request, "vunireg/event_edit_competition.html", content)
    
# Funktion, die die Seite zum Bearbeiten der Mindestqualifikationen zurückgibt
def event_edit_qualification(request, event_id, qualification_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    qualification_list = Qualification.objects.filter(event__id=event.pk).order_by('maxage')
    
    
    qualification_table = []
    # Erste Reihe: 'Alter' / Disziplin 1 / Disziplin 2 / ...
    row = [ 'Alter' ]
    disciplines = Discipline.objects.filter(qualification__in=Qualification.objects.filter(event__id=event.pk)).distinct()
    for discipline in disciplines:
        row.append(discipline.name)
    print(row)
    qualification_table.append(row)
    # Weitere Reihen Reihe: Alter von ... bis ... / Quali Disziplin 1 / Quali Disziplin 2 / ...
    agetuples = (Qualification.objects.values_list('minage', 'maxage').order_by('minage').distinct())
    print(agetuples)
    for agetuple in agetuples:
        print(agetuple)
        if (agetuple[1] < 99):
            row = [ str(agetuple[0]) + ' - ' + str(agetuple[1]) + ' Jahre']
        else:
             row = [ str(agetuple[0]) + ' Jahre und älter']
        for discipline in disciplines:
            try:
                tempqualification = qualification_list.get(discipline__id=discipline.pk, minage=agetuple[0], maxage=agetuple[1])
                row.append(tempqualification.value)
            except Qualification.DoesNotExist:
                row.append('')
        print(row)
        qualification_table.append(row)
    print(qualification_table)
    
    if request.method == 'POST':
        form = QualificationEditForm(request.POST)
        if form.has_changed():
            if form.is_valid():
                newqualification = form.save(commit=False)
                newqualification.event = event
                newqualification.save()
                response += 'Die Änderungen wurden erfolgreich gespeichert!'
            else:
                error += 'Das Formular konnte nicht validiert werden!'
    elif not qualification_id == 'None':
        qualification = get_object_or_404(Qualification, pk=qualification_id)
        form = QualificationEditForm(instance = qualification)
    else:
        form = QualificationEditForm()
    # Ausgabe
    content = {
        'qualification_table':qualification_table, 'form':form, 'event':event, 'error':error, 'response':response,
    }
    return render(request, 'vunireg/event_edit_qualification.html', content)