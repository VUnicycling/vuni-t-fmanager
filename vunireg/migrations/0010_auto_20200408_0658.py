# Generated by Django 3.0.5 on 2020-04-08 06:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vunireg', '0009_auto_20200408_0633'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='competition',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='vunireg.Competition'),
        ),
        migrations.AlterField(
            model_name='start',
            name='entry',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='vunireg.Entry'),
        ),
    ]
