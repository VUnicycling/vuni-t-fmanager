from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
import datetime
import csv
import types
import serial
import serial.tools.list_ports
from django.db.models import F, Func

from django.template.loader import render_to_string
from django.conf import settings
from django.contrib import messages
import tempfile

from vunireg.models import Event, Person, Club, Entry, Discipline, Agegroup, Competition, Start, Scoringcategory, Qualification, Result
from .filters import PersonFilter, ClubFilter, EntryFilter, CompetitionFilter
from .forms import EventEditForm, PersonEditForm, EntryEditInlineForm, ScoringcatEditForm, QualificationEditForm, ResultEditInlineFormset

# Globale Variable für die Serielle Verbindung
ser = serial.Serial()

# Funktion zum Testes der Seriellen Verbindung
def infield_serialtest():
#    ports = serial.tools.list_ports.comports()
#    for port, desc, hwid in sorted(ports):
#        print("{}: {} [{}]".format(port, desc, hwid))
    global ser
    try:
        ser = serial.Serial(
            port = "/dev/cu.usbserial",
            baudrate = 9600,
            bytesize = serial.EIGHTBITS,
            parity = serial.PARITY_NONE,
            stopbits = serial.STOPBITS_ONE,
            timeout = 1
        )
        ser.isOpen()
        print("Serielle Schnitstelle ist geöffnet.")
        return 1
    except IOError:
        print("Serielle Schnittstelle konnte nicht geöffnet werden.")
        ser.close()
        return 0
        
# Funktion zum Senden von Daten über die Serielle Schnittstelle
def infield_serialsend(request):
    data = request.GET.get('data')
    global ser
    data = 's'
    # ToDo - Überprüfen, ob die Anlage bereit für einen Start ist
    ser.write(bytes(data.encode()))
    print('Folgende Daten wurden an die Serielle Schnittstelle gesendet: ' + data)
    return HttpResponse('Daten gesendet')

# Funktion zum Lesen von Daten aus der Seriellen Schnittstelle
def infield_serialread(request):
    global ser
    response = ''
    data = str(ser.readline(), 'utf-8')
    while not(data == ''):
        print('Folgende Daten wurden empfangen: ' + data)
        if data == 'Time:\n':
            print('Zeit erkannt')
            data = str(ser.readline(), 'utf-8')
            print(data)
            response = data
        data = str(ser.readline(), 'utf-8')
    return HttpResponse(response)

# Funktion, die die Seite für die Infield Judges beim IUF Slalom zurückgibt
def infield_iuf_slalom(request, event_id, start_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    system = types.SimpleNamespace()
    if infield_serialtest() == 1:
        system.state = 1
    else:
        system.state = 0
    system.stateLS1 = -1
    system.stateLS2 = -1
    system.stateLS3 = -1
    system.stateLS4 = -1
    start_list = Start.objects.filter(event__id=event.pk, competition__discipline__id=8, competition__round=1).order_by('heat', 'lane')
    # Bestimmen des Teilnehmers für die Anzeige im Frontend
    if not start_id == 'None':
        start = get_object_or_404(Start, pk=start_id)
        result_list = start.result_set.all().order_by('timestamp')
    else:
        start = Start.objects.none()
        result_list = None
        results_count  = 0
    if request.method == "POST":
        if 'load_person' in request.POST:
            startnr = request.POST.get('startnr')
            if not startnr == '':
                try:
                    person = Person.objects.get(startnr=startnr)
                    try:
                        start = start_list.get(person__startnr=startnr)
                    except Start.DoesNotExist:
                        messages.error(request, 'Kein Start für die angegebene Startnummer vorhanden!')
                        return redirect('vunireg:infield_iuf_slalom', event_id = event.pk, start_id = 'None')
                except Person.DoesNotExist:
                    messages.error(request, 'Die angegebene Startnummer ist nicht vergeben!')
                    return redirect('vunireg:infield_iuf_slalom', event_id = event.pk, start_id = 'None')
                return redirect('vunireg:infield_iuf_slalom', event_id = event.pk, start_id = start.pk)
            else:
                messages.error(request, 'Keine Startnummer eingegeben!')
        if 'unload_person' in request.POST:
            start = Start.objects.none()
            return redirect('vunireg:infield_iuf_slalom', event_id = event.pk, start_id = 'None')
        if 'save_result_valid' in request.POST:
            input_value = request.POST.get('inputchronotime')
            print(input_value)
            if not input_value == 'None':
                result = Result.objects.create(event=event, person=start.person, discipline=start.competition.discipline, start=start, judge=0, value=input_value, custom2='valid', timestamp=datetime.datetime.now())
                messages.success(request, 'Das Ergebnis wurde gespeichert!')
            else:
                messages.error(request, 'Keine Gültige Zeit! Kein Ergebnis gespeichert!')
        if 'save_result_invalid' in request.POST:
            result = Result.objects.create(event=event, person=start.person, discipline=start.competition.discipline, start=start, judge=0, value='DQ', custom2='DQ', timestamp=datetime.datetime.now())
            messages.success(request, 'Das Ergebnis wurde gespeichert!')
    # Ausgabe
    if not start_id == 'None':
        results_count  = result_list.count()
        if results_count == 2:
            messages.error(request, 'Es sind bereits zwei Ergebnisse gespeichert - es können keine weiteren hinzugefügt werden!' )
    if system.state == 0:
        messages.error(request, 'Das System ist nicht verbunden - DEMO MODUS aktiv!')
    content = {
        'event':event, 'response':response, 'error':error, 'system':system, 'start':start, 'start_list':start_list, 'result_list':result_list
    }
    return render(request, "vunireg/infield/infield_iuf_slalom.html", content)