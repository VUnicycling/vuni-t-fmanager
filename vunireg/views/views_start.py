from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
import datetime
import csv
import types
from django.db.models import F, Func

from django.template.loader import render_to_string
from django.conf import settings
from weasyprint import HTML, CSS
import tempfile

from vunireg.models import Event, Person, Club, Entry, Discipline, Agegroup, Competition, Start, Scoringcategory, Qualification, Result
from .filters import PersonFilter, ClubFilter, EntryFilter, CompetitionFilter
from .forms import EventEditForm, PersonEditForm, EntryEditInlineForm, ScoringcatEditForm, QualificationEditForm, ResultEditInlineFormset


# Funktion, die die Übersichtsseite für die Starts zurückgibt
def start(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    # Ermitteln welche Disziplinen es bei der Veranstaltung gibt
    disciplinequery = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
    # Warnmeldung ausgeben, falls es noch Meldungen gibt, die keinem Wettbewerb zugeordnet sind
    # Alle Meldungen des Events berücksichtigen, abzüglich der Staffelmeldungen, die von einer Person kommen und nicht zu einem Team gehören
    entry_query = Entry.objects.filter(event__id=event.pk)
    entrys_for_starts_query = Entry.objects.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_starts_query.filter(competition=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Meldung(en), die keinem Wettbewerb zugeordnet sind. Diese wird/werden beim Setzten der Starts NICHT berücksichtigt!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Staffel-Meldungen gibt, die keiner Staffel zugeordnet sind
    entrys_for_teams_query = entry_query.filter(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_teams_query.filter(entry_team=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Einzel-Meldung(en) für eine Staffel, die keinem Staffel-Team zugeordnet ist/sind!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Starter gibt, die keine Startnummer haben
    startnr_error = len(Person.objects.filter(entry__in=entry_query, startnr=None).distinct())
    if startnr_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(startnr_error) + ' Starter, dem/denen keine Startnummer(n) zugeordnet ist/sind!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Altersklassen-Wettbewerbe gibt, die weniger als 6 Meldungen haben
    competition_error = len(Competition.objects.filter(ismerged = False, round = 1, entrycount__lte = 5, event__id = event.pk))
    if competition_error > 0:
        error += 'HINWEIS: Es gibt noch ' + str(competition_error) + ' Altersklassen-Wettbewerb(e), mit weniger als sechs Melungen!' + '<br />'
    # Colorpick State für die Disziplinen ermitteln
    for temp_discipline in disciplinequery:
        tempentry_list = entry_query.filter(discipline=temp_discipline)
        # Bei Team-Disziplinen müssen die Meldungen anders berücksichtigt werden
        if temp_discipline.isgroupdisc >= 1:
            tempentry_list = tempentry_list.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
            tempentry_list = tempentry_list.filter(entry_team=None)
        # Setzten des Status-Fags
        if len(tempentry_list) == 0:
            temp_discipline.state = 'NOENTRYS'
        elif len(tempentry_list.filter(competition=None)) == 0 and len(tempentry_list.exclude(start=None)) == 0:
            temp_discipline.state = 'NOSTARTS'
        elif len(tempentry_list.filter(competition=None)) > 0 or ((len(tempentry_list.exclude(start=None)) > 0 and len(tempentry_list.filter(start=None)) > 0)):
            temp_discipline.state = 'ERROR'
        else:
            temp_discipline.state = 'STARTSOK'
    # Eine Liste aller Vereine erstellen, für die Vereinsweise Kontrollübersicht der Starts
    club_list = Club.objects.filter(person__in=Person.objects.filter(entry__in=entry_query)).distinct()
    if request.method == "POST":
        club_id = request.POST.get('club')
        start_list = Start.objects.filter(person__club__id=club_id).order_by('person', 'competition')
        content = {
            "start_list":start_list, 'event':event, 'discipline_list':disciplinequery
        }
        return render(request, "vunireg/start_club2.html", content)
    # Ausgabe
    content = {
        'event':event, 'disciplinequery':disciplinequery, 'club_list':club_list, 'response':response, 'error':error
    }
    return render(request, "vunireg/start_overview.html", content)

# Funktion, die eine Listenansicht aller Wettbewerbe der Veranstaltung ausgibt
def competition_list(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    ag_competition_list = Competition.objects.filter(event__id=event.pk, round=1).order_by('discipline', 'gender', 'maxage')
    final_competition_list = Competition.objects.filter(event__id=event.pk, round=2).order_by('discipline', 'gender', 'maxage')
    ag_competition_filter = CompetitionFilter(request.GET, queryset=ag_competition_list)
    final_competition_filter = CompetitionFilter(request.GET, queryset=final_competition_list)
    # Meldungen den AK-Wettbewerben zuteilen
    if(request.GET.get('match_entrys')):
        print('Match Entrys')
        match_entrys(event_id)
    # AK-Wettbewerbe zusammenlegen
    if(request.GET.get('merge_competitions')):
        print('Merge Competitions')
        merge_competitions(event_id, 0, 10)
        merge_competitions(event_id, 11, 100)
    # Zusammenlegen von AK-Wettbewerben auflösen
    if(request.GET.get('unmerge_competitions')):
        print('Unmerge Competitions')
        unmegre_all(event_id)
    # Warnmeldung ausgeben, falls es noch Altersklassen-Wettbewerbe gibt, die weniger als 6 Meldungen haben
    competition_error = len(ag_competition_list.filter(ismerged=False, entrycount__lte=5))
    if competition_error > 0:
        error += 'HINWEIS: Es gibt noch ' + str(competition_error) + ' Altersklassen-Wettbewerb(e), mit weniger als sechs Melungen!' + '<br />'
    # Ausgabe
    content = {
        'filter':ag_competition_filter, 'final_filter':final_competition_filter, 'event':event, 'response':response, 'error':error,
    }
    return render(request, "vunireg/competition_list.html", content)

# Funktion, die die Seite für das Vergeben der Startnummern zurückgibt
def start_startnr(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    startnr_error = 0
    # Ermitteln welche Disziplinen es bei der Veranstaltung gibt
    discipline_query = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
    # Ermitteln aller Teilnehmer
    entry_list = Entry.objects.filter(event__id=event.pk)
    person_query = Person.objects.filter(entry__in=entry_list).distinct().order_by('startnr')
    # Auswerten von POST-Daten
    if request.method == "POST":
        # Zuweisen der Startnummern
        if 'set_startnr' in request.POST:
            fromnr = int(request.POST.get('fromnr'))
            order = request.POST.get('order')
            order2 = request.POST.get('order2')
            data = request.POST.get('leaffree')
            leaffree = [int(x) for x in data.split(',') if x]
            startnr = fromnr
            # Ermitteln, ob es Starter ohne Startnummer gibt
            persons_without_stnr = person_query.filter(startnr=None).order_by(order, order2)
            # Es gibt keine Starter ohne Startnummer
            if not persons_without_stnr:
                response += 'Es ist bereits allen Startern eine Startnummer zugeordnet.' + '</br>'
            # Es gibt noch Starter ohne Startnummer
            else:
                # leaffree um bereits vergebene Startnummern erweitern
                persons_with_stnr = person_query.exclude(startnr=None).order_by('startnr')
                for person in persons_with_stnr:
                    leaffree.append(person.startnr)
                # Personen ohne Startnummer entsprechend der gewünschten Reihenfolge Startnummern zuweisen
                for person in persons_without_stnr:
                    while( startnr in leaffree):
                        startnr += 1
                    person.startnr = startnr
                    person.save()
                    startnr += 1
                response += 'Allen Startern wurde eine Startnummer zugeordnet.' + '</br>'
        # Zuweisung aller Startnummern löschen
        elif 'del_startnr' in request.POST:
            for person in person_query:
                person.startnr = None
                person.save()
            response += 'Alle Startnummern wurden gelöscht.' + '</br>'
    # Warnmeldung ausgeben, wenn es noch Teilnehmer ohne Startnummer gibt
    persons_without_stnr = person_query.filter(startnr = None)
    startnr_error = persons_without_stnr.count()
    if startnr_error > 0:
        error += 'AUCHTUNG: Es gibt noch ' + str(startnr_error) + ' Starter, dem/denen keine Startnummer(n) zugeordnet ist/sind!' + '</br>'
    # Warnmeldung ausgeben, wenn es keine Teilnehmer mit Meldungen für die Veranstaltung gibt
    if not person_query:
        error += 'AUCHTUNG: Es gibt keine Teilnehmer mit Meldungen für diese Veranstaltung!' + '</br>'
    # Ausgabe
    content = {
        'person_list':person_query, 'event':event, 'response':response, 'error':error
    }
    return render(request, "vunireg/start_startnr.html", content)

# Funktion, die einzelne Startnummern löscht
def startnr_clr(request, event_id, person_id):
    person = get_object_or_404(Person, pk=person_id)
    response = ''
    error = ''
    person.startnr = None
    person.save()
    response += 'Die Startnummer für den Starter/ die Starterin ' + person.forename + ' ' + person.surname + ' wurde zurückgesetzt.' + '</br>'
    return redirect("vunireg:start_startnr", event_id)

# Funktion, die die Übersichtsseite für die Seite zum Setzten der Starts zurückgibt
def start_seed_overview(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    # Ermitteln welche Disziplinen es bei der Veranstaltung gibt
    disciplinequery = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
    # Warnmeldung ausgeben, falls es noch Meldungen gibt, die keinem Wettbewerb zugeordnet sind
    # Alle Meldungen des Events berücksichtigen, abzüglich der Staffelmeldungen, die von einer Person kommen und nicht zu einem Team gehören
    entry_query = Entry.objects.filter(event__id=event.pk)
    entrys_for_starts_query = Entry.objects.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_starts_query.filter(competition=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Meldung(en), die keinem Wettbewerb zugeordnet sind. Diese wird/werden beim Setzten der Starts NICHT berücksichtigt!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Staffel-Meldungen gibt, die keiner Staffel zugeordnet sind
    entrys_for_teams_query = entry_query.filter(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_teams_query.filter(entry_team=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Einzel-Meldung(en) für eine Staffel, die keinem Staffel-Team zugeordnet ist/sind!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Starter gibt, die keine Startnummer haben
    startnr_error = len(Person.objects.filter(entry__in=entry_query, startnr=None).distinct())
    if startnr_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(startnr_error) + ' Starter, dem/denen keine Startnummer(n) zugeordnet ist/sind!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Altersklassen-Wettbewerbe gibt, die weniger als 6 Meldungen haben
    competition_error = len(Competition.objects.filter(ismerged=False, round=1, entrycount__lte=5, event__id=event.pk))
    if competition_error > 0:
        error += 'HINWEIS: Es gibt noch ' + str(competition_error) + ' Altersklassen-Wettbewerb(e), mit weniger als sechs Melungen!' + '<br />'
    # Colorpick State für die Disziplinen ermitteln
    for temp_discipline in disciplinequery:
        tempentry_list = entry_query.filter(discipline=temp_discipline)
        # Bei Team-Disziplinen müssen die Meldungen anders berücksichtigt werden
        if temp_discipline.isgroupdisc >= 1:
            tempentry_list = tempentry_list.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
            tempentry_list = tempentry_list.filter(entry_team=None)
        # Setzten des Status-Fags
        if len(tempentry_list) == 0:
            temp_discipline.state = 'NOENTRYS'
        elif len(tempentry_list.filter(competition=None)) == 0 and len(tempentry_list.exclude(start=None)) == 0:
            temp_discipline.state = 'NOSTARTS'
        elif len(tempentry_list.filter(competition=None)) > 0 or ((len(tempentry_list.exclude(start=None)) > 0 and len(tempentry_list.filter(start=None)) > 0)):
            temp_discipline.state = 'ERROR'
        else:
            temp_discipline.state = 'STARTSOK'
    # Ausgabe
    content = {
        'event':event, 'disciplinequery':disciplinequery, 'response':response, 'error':error
    }
    return render(request, "vunireg/start_seed_overview.html", content)


# Funktion, welche überprüft, ob die Mindestqualifikation für eine übergebene Disziplin, Alter und Geschlecht erfüllt ist
def check_quali(discipline, age, gender, result):
    try:
        quali = Qualification.objects.get(discipline = discipline, minage__lte = age, maxage__gte = age)
        qualivalue = quali.value
    except:
        return 1
    if(qualivalue and discipline.ranking == 'ASC'):
       if(result <= qualivalue):
           return 1
       else:
           return 0
    elif(qualivalue and discipline.ranking == 'DSC'):
       if(result >= qualivalue):
           return 1
       else:
           return 0
    else:
       return 1

# Funktion, zur Bestimmen der Finalteilnehmer
def get_finalists(result_query, ranking, isgroupdisc, usequali, finalcompetitors):
    if ranking == 'ASC':
        results = result_query.order_by(F('value').asc(nulls_last=True))
    else:
        results = result_query.annotate(num_value=Cast('value', FloatField())).order_by(F('num_value').desc(nulls_last=True))
    current_rank = 1
    counter = 0
    for result in results:
        ### Für das Finale werden nur gültige Ergebnis berücksichtig
        if result.value == None:
            results = results.exclude(id=result.pk)
        else:
            ### Überprüfen, ob die Qualifikation erreicht wurde
            if(usequali):
                if isgroupdisc == 1:
                    quali_ok = check_quali(result.start.competition.discipline, result.start.person.age_at(result.start.entry.event.startdate), result.start.person.gender, result.value)
                else:
                    quali_ok = check_quali(result.start.competition.discipline, result.start.entry.team.ageforcomp, result.start.competition.gender, result.value)
            else:
                quali_ok = 1
            if counter < 1:
                if (quali_ok):
                    result.rank = current_rank
                    counter += 1
                else:
                    results = results.exclude(id=result.pk)
            else:
                if (quali_ok):
                    if result.value == results[counter - 1].value:
                        ### To Do ###
                        ### Überprüfen, ob das Fotofinish über die Reihenfolge entscheiden kann (Plätze 1-3)
                        ### To Do ###
                        result.rank = current_rank
                        counter += 1
                    else:
                        current_rank = counter + 1
                        result.rank = current_rank
                        counter += 1
                else:
                    results = results.exclude(id=result.pk)
        ### Abbruch, wenn die maximale Anzahl an Finalteilnehmern erreicht ist
        if current_rank > finalcompetitors:
            results = results.all()[:counter-1]
            break
    # Rückgabe der Finalisten
    finalists = result_query.filter(id__in=results)
    if ranking == 'ASC':
        finalists = finalists.order_by(F('value').asc(nulls_last=True))
    else:
        finalists = finalists.annotate(num_value=Cast('value', FloatField())).order_by(F('num_value').desc(nulls_last=True))
    return finalists

# Funktion, die eine Listenansicht aller gesetzten Starts für eine Disziplin zurückgibt
def start_seed_listview(request, event_id, discipline_id):
    event = get_object_or_404(Event, pk=event_id)
    discipline = get_object_or_404(Discipline, pk=discipline_id)
    response = ''
    error = ''    
    # Ermitteln welche Disziplinen es bei der Veranstaltung gibt
    disciplinequery = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
    competition_list = Competition.objects.filter(event__id=event.pk, discipline__id=discipline_id).order_by('-maxage', 'gender')
    start_list = Start.objects.filter(event__id=event.pk, competition__discipline__id=discipline_id, competition__round=1).order_by('heat', 'lane')
    start_list_final = Start.objects.filter(event__id=event.pk, competition__discipline__id=discipline_id, competition__round=2).order_by('heat', 'lane')
    # Status der Disziplin (Final: ja/nein; Finale setztbar, etc.)
    discipline_state = types.SimpleNamespace()
    if competition_list.filter(round=2).count() > 0:
        discipline_state.hasfinals = 1
    if request.method == "POST":
        # AK Starts als CSV exportieren
        if 'export_csv' in request.POST:
            export_csv(start_list)
            print(export_csv)
        # AK Starts löschen
        if 'del_seeding' in request.POST:
            for start in start_list:
                start.delete()
            start_list = Start.objects.filter(event__id = event.pk, competition__discipline__id = discipline_id, competition__round=1).order_by('heat', 'lane')
            response += 'Alle Altersklassen-Starts für die Disziplin ' + discipline.name + ' gelöscht!' + '<br />'
        # AK Starts setzten
        if 'seeding' in request.POST:
            if discipline.custom1 == 'ILS':
                seed_straightlanefull(discipline_id, event_id, 8)
                response += str(start_list.count()) + ' Altersklassen-Starts für die Disziplin ' + discipline.name + ' gesetzt.' + '<br />'
            elif discipline.custom1 == 'ILR':
                seed_roundlanefull(discipline_id, event_id, 8)
                response += str(start_list.count()) + ' Altersklassen-Starts für die Disziplin ' + discipline.name + ' gesetzt.' + '<br />'
            elif discipline.custom1 == 'FRE':
                seed_freestart(discipline_id, event_id)
                response += str(start_list.count()) + ' Altersklassen-Starts für die Disziplin ' + discipline.name + ' gesetzt.' + '<br />'
            else:
                error += 'Für die Disziplin ' + discipline.name + ' ist kein gültiges Setzverfahren angegeben!' + '<br />'
        ### TODO ###
        # Final Starts setzten
        if 'seeding_finals' in request.POST:
            # Anzahl an Final-Teilnehmern bestimmen (i.d.R. die Anzahl der verfügbaren Bahnen)
            if discipline.custom1 == 'ILS':
                finalcompetitors = event.lanesstr
            elif discipline.custom1 == 'ILR':
                finalcompetitors = event.lanesrnd
            elif discipline.custom1 == 'FRE':
                finalcompetitors = 8
            else:
                error += 'Für die Disziplin ' + discipline.name + ' ist kein Default-Wert angegeben, wie viele Final-Teilnehmern es geben soll!' + '<br />'
            response += 'Für jedes Finale der Disziplin ' + discipline.name + ' sollten ' + str(finalcompetitors) +  ' Starts gesetzt werden.' + '<br />'
            # Zu setzende Finals bestimmen (i.d.R. Juniorfinale und Finale)
            finals_to_set = Competition.objects.filter(event__id=event.pk, discipline__id=discipline_id, round=2).order_by('maxage', 'gender')
            # Bestimmen der Gesamtwertungen entsprechend der Alterseinteilung der Finals (ggf. bereits nach der Anzahl der Finalteilnehmer abbrechen)        
            # Schleifenzähler für das korrekte Nummerieren der Finalläufe
            heat_to_set = 1
            for final in finals_to_set:
                # Wertungsfilter
                scoringcat = final.scoringcat
                filtertype1 = scoringcat.filtertype1
                filtervalue1 = scoringcat.filtervalue1
                filtertype2 = scoringcat.filtertype2
                filtervalue2 = scoringcat.filtervalue2
                minage = scoringcat.minage
                maxage = scoringcat.maxage
                gender = final.gender
                # Ergebnisse ermitteln, die für dieses Finale ausgewertet werden müssen
                result_query = Result.objects.filter(start__event__id=event.pk, start__competition__discipline__id=discipline.pk, start__competition__round=1, start__competition__gender=gender)
                for result in result_query:
                    # Bei mixed Disziplinen (m&w) muss das Alter des Teams für die Zuordnung berücksichtigt werden
                    if (discipline.mixedgender == True):
                        if (result.start.entry.team.ageforcomp < minage):
                            result_query = result_query.exclude(id=result.pk)
                        elif (result.start.entry.team.ageforcomp > maxage):
                            result_query = result_query.exclude(id=result.pk)
                    # Bei allen anderen Disziplinen muss das Alter des Teilnehmers für die Zuordnung berücksichtigt werden
                    else:
                        # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
                        if ((maxage <= 10) and (result.start.person.age_at(event.startdate) <= maxage) and (result.start.person.otheragegroup == True)):
                            result_query = result_query.exclude(id=result.pk)
                            print('U11: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
                        elif (result.start.person.age_at(event.startdate) < minage):
                            if not ((minage == 11) and (result.start.person.otheragegroup == True)):
                                result_query = result_query.exclude(id=result.pk)
                            else:
                                print('11+: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
                        elif (result.start.person.age_at(event.startdate) > maxage):
                            result_query = result_query.exclude(id=result.pk)
                # Bei mixed Disziplinen (m&w) wird auf Basis des Teams gefiltert
                if (discipline.mixedgender == True):
                    ### TODO ###
                    # Wertungs-Filter für mixed Disziplinen implementieren
                    rank_query = result_query
                    withoutrank_query = Result.objects.none()
                    ### ENDTODO ###
                # Bei allen anderen Disziplinen wird auf Basis der Personen gefiltert
                else:
                    if not filtertype1 == None:
                        kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
                        rank_query = result_query.filter(**kwargs)
                        withoutrank_query = result_query.exclude(**kwargs)
                    elif not filtertype2 == None:
                        kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
                        rank_query = result_query.filter(**kwargs)
                        withoutrank_query = result_query.exclude(**kwargs)
                    else:
                        rank_query = result_query
                        withoutrank_query = Result.objects.none()
                # Wertungen auf Disziplinenbasis (pro Disziplin, sofern es keine Mixed Disziplin ist nur zwischen 'männlich' und 'weiblich' unterschieden, alle Wettbewerbe der Runde 1/AK)
                # Bei Radlauf müssen U11 Fahrer aus der Juniorwertung ausgeschlossen werden
                if discipline.pk == 7 and minage <= 10:
                    for result in rank_query:
                        if result.start.person.age_at(dateforage) < 11 and (not(result.start.person.otheragegroup == True)):
                            rank_query = rank_query.exclude(id=result.pk)
                printcomp = final.discipline.name + ' ' + final.fullname
                finalists = get_finalists(rank_query, discipline.ranking, discipline.isgroupdisc, scoringcat.usequali, finalcompetitors)
                # Durch Gleichstände können mehr Teilnehmer ein Anrecht auf's Finale haben als Finalisten möglich sind
                if len(finalists) == 0:
                    error += 'Für das ' + final.discipline.name + ' ' + final.fullname +' gibt es keine Qualifizierten Teilnehmer!' + '<br />'
                elif len(finalists) > finalcompetitors:
                    error += 'Das ' + final.discipline.name + ' ' + final.fullname +' kann nicht automatisch gesetzt werden, da duch Gleichstand mehr Teilnehmer ein Anrecht auf das Finale haben als Finalisten möglich sind!' + '<br />'
                # Wenn bereits Starts gesetzt sind kann nicht mehr automatisch gesetzt werden
                elif len(final.start_set.all()) > 0:
                    error += 'Das ' + final.discipline.name + ' ' + final.fullname +' kann nicht automatisch gesetzt werden, da bereits Starts für dieses Finale gesetzt sind!' + '<br />'
                # Setzten des Finallaufes
                else:
                    if discipline.custom1 == 'ILS':
                        seed_query_straightlanefull(final.pk, finalists, 'result' ,event.lanesstr, heat_to_set)
                        response += str(finalists.count()) + ' Starts für das ' + final.discipline.name + ' ' + final.fullname + ' gesetzt.' + '<br />'
                    elif discipline.custom1 == 'ILR':
                        #seed_roundlanefull(discipline_id, event_id, 8)
                        response += str(finalists.count()) + ' Starts für das ' + final.discipline.name + ' ' + final.fullname + ' gesetzt.' + '<br />'
                    elif discipline.custom1 == 'FRE':
                        #seed_freestart(discipline_id, event_id)
                        response += str(finalists.count()) + ' Starts für das ' + final.discipline.name + ' ' + final.fullname + ' gesetzt.' + '<br />'
                    else:
                        error += 'Für die Disziplin ' + discipline.name + ' ist kein gültiges Setzverfahren angegeben!' + '<br />'
                # Schleifenzähler erhöhen
                print(heat_to_set)
                heat_to_set += 1
        ### ENDTODO ###
        # Final Starts löschen
        if 'del_seeding_finals' in request.POST:
            for start in start_list_final:
                start.delete()
            start_list_final = Start.objects.filter(event__id=event.pk, competition__discipline__id=discipline_id, competition__round=2).order_by('heat', 'lane')
            response += 'Alle Final Starts für die Disziplin ' + discipline.name + ' gelöscht!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Meldungen für die aktuelle Disziplin gibt, die keinem Wettbewerb zugeordnet sind
    # Alle Meldungen des Events berücksichtigen, abzüglich der Staffelmeldungen, die von einer Person kommen und nicht zu einem Team gehören
    entry_query = Entry.objects.filter(event__id=event.pk)
    discipline_entry_query = entry_query.filter(discipline__id=discipline_id)
    entrys_for_starts_query = discipline_entry_query.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_starts_query.filter(competition=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Meldung(en), die keinem Wettbewerb zugeordnet sind. Diese wird/werden beim Setzten der Starts NICHT berücksichtigt!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Staffel-Meldungen gibt, die keiner Staffel zugeordnet sind
    entrys_for_teams_query = discipline_entry_query.filter(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_teams_query.filter(entry_team=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Einzel-Meldung(en) für eine Staffel, die keinem Staffel-Team zugeordnet ist/sind!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Starter gibt, die keine Startnummer haben
    startnr_error = len(Person.objects.filter(entry__in=discipline_entry_query, startnr=None).distinct())
    if startnr_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(startnr_error) + ' Starter, dem/denen keine Startnummer(n) zugeordnet ist/sind!' + '<br />'
    # Warnmeldung ausgeben, falls es noch Altersklassen-Wettbewerbe gibt, die weniger als 6 Meldungen haben
    competition_error = len(Competition.objects.filter(discipline__id=discipline_id, ismerged=False, round=1, entrycount__lte=5, event__id=event.pk))
    if competition_error > 0:
        error += 'HINWEIS: Es gibt noch ' + str(competition_error) + ' Altersklassen-Wettbewerb(e), mit weniger als sechs Melungen!' + '<br />'
    # Colorpick State für die Disziplinen ermitteln
    for temp_discipline in disciplinequery:
        tempentry_list = entry_query.filter(discipline=temp_discipline)
        # Bei Team-Disziplinen müssen die Meldungen anders berücksichtigt werden
        if temp_discipline.isgroupdisc > 1:
            tempentry_list = tempentry_list.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
            tempentry_list = tempentry_list.filter(entry_team=None)
        # Setzten des Status-Fags
        if len(tempentry_list) == 0:
            temp_discipline.state = 'NOENTRYS'
        elif len(tempentry_list.filter(competition=None)) == 0 and len(tempentry_list.exclude(start=None)) == 0:
            temp_discipline.state = 'NOSTARTS'
        elif len(tempentry_list.filter(competition=None)) > 0 or ((len(tempentry_list.exclude(start=None)) > 0 and len(tempentry_list.filter(start=None)) > 0)):
            temp_discipline.state = 'ERROR'
        else:
            temp_discipline.state = 'STARTSOK'    
    # Ausgabe
    content = {
        'competition_list':competition_list, 'start_list':start_list, 'start_list_final':start_list_final, 'activediscipline':discipline, 'discipline_state':discipline_state, 'event':event, 'disciplinequery':disciplinequery, 'response':response, 'error':error
    }
    return render(request, "vunireg/start_seed_listview.html", content)

### To Do beim Setzten der Läufe ###
### Übergabe, ob möglicherweise Bahnen frei gelassen werden sollen (nicht nutzbar wegen Defekten oder aus Abstandsgründen (Staffel)
### To Do beim Setzten der Läufe ###

# Funktion, die eine übergebene Query (Meldungen(unsortiert) oder Ergebnisse(sortiert)) auf der Sprintbahn entsprechend dem IUF Regelwerk setzt
def seed_query_straightlanefull(competition_id, toseed_query, typetoseed, lanes, firstheat):
    competition = get_object_or_404(Competition, pk=competition_id)
    event = competition.event
    # Bestimmen, welche Bahn zuerst gesetzt werden muss
    nuberoflanes = lanes
    firstsetlane = int(nuberoflanes/2) + (nuberoflanes % 2 > 0)
    # Daten sortieren und ermitteln, wie viele Starts und Läufe gesetzt werden müssen
    if typetoseed == 'entry':
        tempseedset = toseed_query.order_by(F('value').asc(nulls_last=True))
    else:
        tempseedset = toseed_query
    nubertoset = len(tempseedset)
    totalheats = nubertoset // nuberoflanes + (nubertoset % nuberoflanes > 0)
    # Variablen zum speichern des ersten und letzten Laufes, der gesetzt wird
    firstsetheat = (firstheat - 1)
    lastsetheat = firstheat
    # Der letzte (schnellste) Lauf des Wettbewerbes wird zuerst gesetzt
    firstsetheat += totalheats
    heattoset = firstsetheat
    # Setzten der Läufe
    while (heattoset >= lastsetheat) and tempseedset.exists():
        lanetoset = firstsetlane
        i = 0
        j = -1
        # Verteilen auf die Bahnen
        while ((lanetoset <= nuberoflanes) and (lanetoset > 0)) and tempseedset.exists():
            # Abbruch, wenn im nächsten Lauf weniger als drei Starts gesetzt würden.
            if nubertoset <= 3 and heattoset > lastsetheat:
                break
            tempdata = tempseedset.first()
            # Unterscheiden, ob anhand von Meldungen oder Ergebnissen gesetzt werden soll
            if typetoseed == 'entry':
                # Wenn für die Daten noch kein Start existiert, Start anlegen
                if not tempdata.start_set.all():
                    start = Start(event=event, person=tempdata.person, entry=tempdata, competition=competition, heat=heattoset, lane=lanetoset, value=tempdata.value)
                    start.save()
                    tempseedset = tempseedset.exclude(id=tempdata.id)
                    i += 1
                    j *= (-1)
                    lanetoset += (i*j)
                    nubertoset -= 1
                # Wenn für die Meldung bereits ein Start existiert, Meldung aus der Liste der noch zu setzenden entfernen
                else:
                     tempseedset = tempseedset.exclude(id=tempentry.id)
            else:
                # Wenn für die Daten noch kein Start im Wettbewerb existiert, Start anlegen
                try:
                    start = Start.objects.get(event=event, person=tempdata.start.person, competition=competition)
                except Start.DoesNotExist:
                    start = Start(event=event, person=tempdata.start.person, competition=competition, heat=heattoset, lane=lanetoset, value=tempdata.value)
                    start.save()
                tempseedset = tempseedset.exclude(id=tempdata.id)
                i += 1
                j *= (-1)
                lanetoset += (i*j)
                nubertoset -= 1
        heattoset -=1
    # Der letzte Lauf, der im nächsten Wettbewerb gesetzt wird entspricht dem letzten aus diesem Wettbewerb + die Anzahl der Läufe in diesem Wettbewerb
    lastsetheat += totalheats
    return
    
    
# Funktion, die die Läufe für alle Disziplinen auf der Sprintbahn entsprechend dem IUF Regelwerk setzt
def seed_straightlanefull(discipline_id, event_id, lanes):
    event = get_object_or_404(Event, pk=event_id)
    # Bestimmen der zu setzenden Disziplin
    seeddiscipline = get_object_or_404(Discipline, pk=discipline_id)
    # Bestimmen, welche Bahn zuerst gesetzt werden muss
    nuberoflanes = lanes
    firstsetlane = int(nuberoflanes/2) + (nuberoflanes % 2 > 0)
    # Variablen zum speichern des ersten und letzten Laufen, der für einen Wettbewerb gesetzt wird
    firstsetheat = 0
    lastsetheat = 1
    # Ermitteln, welche Wettbewerbe zur Disziplin gehören und gesetzt werden müssen
    competition_list = Competition.objects.filter(event__id=event_id, discipline=seeddiscipline, ismerged=False).order_by('-maxage')
    # Alle Läufe werden getrennt nach Wettbewerben gesetzt
    for competition in competition_list:
        # Ermitteln, für welche Meldungen in diesem Wettbewerb Starts angelegt und gesetzt werden müssen
        tempentryset = competition.entry_set.all().order_by(F('value').asc(nulls_last=True))
        entrystoset = competition.entrycount
        totalheats = entrystoset // nuberoflanes + (entrystoset % nuberoflanes > 0)
        # Der letzte (schnellste) Lauf des Wettbewerbes wird zuerst gesetzt
        firstsetheat += totalheats
        heattoset = firstsetheat
        # Setzten der Läufe
        while (heattoset >= lastsetheat) and tempentryset.exists():
            lanetoset = firstsetlane
            i = 0
            j = -1
            # Verteilen der Meldungen auf die Bahnen
            while ((lanetoset <= nuberoflanes) and (lanetoset > 0)) and tempentryset.exists():
                # Abbruch, wenn im nächsten Lauf weniger als drei Starts gesetzt würden.
                if entrystoset <= 3 and heattoset > lastsetheat:
                    break
                tempentry = tempentryset.first()
                # Wenn für die Meldung noch kein Start existiert, Start anlegen
                if not tempentry.start_set.all():
                    start = Start(event=event, person=tempentry.person, entry=tempentry, competition=competition, heat=heattoset, lane=lanetoset, value=tempentry.value)
                    start.save()
                    tempentryset = tempentryset.exclude(id=tempentry.id)
                    i += 1
                    j *= (-1)
                    lanetoset += (i*j)
                    entrystoset -= 1
                # Wenn für die Meldung bereits ein Start existiert, Meldung aus der Liste der noch zu setzenden entfernen
                else:
                    tempentryset = tempentryset.exclude(id=tempentry.id)
            heattoset -=1
        # Der letzte Lauf, der im nächsten Wettbewerb gesetzt wird entspricht dem letzten aus diesem Wettbewerb + die Anzahl der Läufe in diesem Wettbewerb
        lastsetheat += totalheats
    return

# Funktion, die die Läufe für alle Disziplinen auf der Rundbahn entsprechend dem IUF Regelwerk setzt
def seed_roundlanefull(discipline_id, event_id, lanes):
    event = get_object_or_404(Event, pk=event_id)
    # Bestimmen der zu setzenden Disziplin
    seeddiscipline = get_object_or_404(Discipline, pk=discipline_id)
    nuberoflanes = lanes
    # Bestimmen, welche Bahn zuerst gesetzt werden muss
    firstsetlane = 1
    # Variablen zum speichern des ersten und letzten Laufes, der für einen Wettbewerb gesetzt wird
    firstsetheat = 0
    lastsetheat = 1
    # Ermitteln, welche Wettbewerbe zur Disziplin gehören und gesetzt werden müssen
    competition_list = Competition.objects.filter(event__id=event_id, discipline=seeddiscipline, ismerged=False).order_by('-maxage')
    # Alle Läufe werden getrennt nach Wettbewerben gesetzt
    for competition in competition_list:
        # Ermitteln, für welche Meldungen in diesem Wettbewerb Starts angelegt und gesetzt werden müssen
        tempentryset = competition.entry_set.all().order_by(F('value').asc(nulls_last=True))
        entrystoset = competition.entrycount
        totalheats = entrystoset // nuberoflanes + (entrystoset % nuberoflanes > 0)
        # Der letzte (schnellste) Lauf des Wettbewerbes wird zuerst gesetzt
        firstsetheat += totalheats
        heattoset = firstsetheat
        # Setzten der Läufe
        while (heattoset >= lastsetheat) and tempentryset.exists():
            lanetoset = firstsetlane
            # Verteilen der Meldungen auf die Bahnen
            while ((lanetoset <= nuberoflanes) and (lanetoset > 0)) and tempentryset.exists():
                # Abbruch, wenn im nächsten Lauf weniger als drei Starts gesetzt würden.
                if entrystoset <= 3 and heattoset > lastsetheat:
                    break
                tempentry = tempentryset.first()
                # Wenn für die Meldung noch kein Start existiert, Start anlegen
                if not tempentry.start_set.all():
                    start = Start(event=event, person=tempentry.person, entry=tempentry, competition=competition, heat=heattoset, lane=lanetoset, value=tempentry.value)
                    start.save()
                    tempentryset = tempentryset.exclude(id=tempentry.id)
                    lanetoset += 1
                    entrystoset -= 1
                # Wenn für die Meldung bereits ein Start existiert, Meldung aus der Liste der noch zu setzenden entfernen
                else:
                    tempentryset = tempentryset.exclude(id=tempentry.id)
            heattoset -=1
        # Der letzte Lauf, der im nächsten Wettbewerb gesetzt wird entspricht dem letzten aus diesem Wettbewerb + die Anzahl der Läufe in diesem Wettbewerb
        lastsetheat += totalheats
    return
    
# Funktion, die die Starts für alle Disziplinen mit offener Startreihenfolge setzt
def seed_freestart(discipline_id, event_id):
    event = get_object_or_404(Event, pk=event_id)
    # Bestimmen der zu setzenden Disziplin
    seeddiscipline = get_object_or_404(Discipline, pk=discipline_id)
    # Ermitteln, welche Wettbewerbe zur Disziplin gehören und gesetzt werden müssen
    competition_list = Competition.objects.filter(event__id=event_id, discipline=seeddiscipline, ismerged=False).order_by('-maxage')
    tempentryset = Entry.objects.none()
    # Alle Meldungen werden zusammen gesetzt
    for competition in competition_list:
        tempentryset = tempentryset | competition.entry_set.all()
        print(tempentryset)
    for tempentry in tempentryset:
        start = Start(event=event, person=tempentry.person, entry=tempentry, competition=competition, value=tempentry.value)
        start.save()
    return

# Funktion zur Ausgabe von CSV Dateien
def export_csv(start_list):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
    
    writer = csv.writer(response)
    
    firststart = start_list.first()
    actual_discipline = firststart.competition.discipline
    actual_heat = firststart.heat
    actual_round = 1
    actual_competition = firststart.competition
    writer.writerow([actual_discipline.code,actual_round,actual_heat,actual_competition.fullname])
    print(actual_discipline.code + ',' + str(actual_round) + ',' + str(actual_heat) + ',' + actual_competition.fullname)
    for start in start_list:
        next_discipline = start.competition.discipline
        next_heat = start.heat
        next_round = 1
        next_competition = start.competition
        if ((actual_discipline != next_discipline) or (actual_heat != next_heat) or (actual_round != next_round) or (actual_competition != next_competition)):
            writer.writerow([])
            writer.writerow([next_discipline.code,next_round,next_heat,next_competition.fullname])
            print()
            print(next_discipline.code + ',' + str(next_round) + ',' + str(next_heat) + ',' + next_competition.fullname)
        writer.writerow(['',start.person.startnr,start.heat])
        print(',' + str(start.person.startnr) + ',' + str(start.lane))
        actual_discipline = start.competition.discipline
        actual_heat = start.heat
        actual_round = 1
        actual_competition = start.competition

    return response

# Funktion zum Ausgeben von PDF-Dateien
def generate_pdf_alt(request):
    """Generate pdf."""
    # Model data
    
    event = get_object_or_404(Event, pk=1)
    scoringcat = get_object_or_404(Scoringcategory, pk=1)
    discipline = get_object_or_404(Discipline, pk=2)
    # Filter für die Wertung bestimmen
    scoringtype = scoringcat.type
    filtertype1 = scoringcat.filtertype1
    filtervalue1 = scoringcat.filtervalue1
    filtertype2 = scoringcat.filtertype2
    filtervalue2 = scoringcat.filtervalue2
    showexternal = scoringcat.showexternal
    minage = scoringcat.minage
    maxage = scoringcat.maxage
    # Ergebnisse ermitteln, die zu dieser Wertung und Disziplin gehören
    result_query = Result.objects.filter(start__competition__discipline__id=discipline.pk)
    for result in result_query:
        # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
        if ((maxage <= 10) and (result.start.person.age_at(event.start) <= maxage) and (result.start.person.otheragegroup == True)):
            result_query = result_query.exclude(id=result.pk)
            print('U11: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
        elif (result.start.person.age_at(event.start) < minage):
            if not ((minage == 11) and (result.start.person.otheragegroup == True)):
                result_query = result_query.exclude(id=result.pk)
            else:
                print('11+: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
        elif (result.start.person.age_at(event.start) > maxage):
            result_query = result_query.exclude(id=result.pk)
    if not filtertype1 == None:
        kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
        rank_query = result_query.filter(**kwargs)
        withoutrank_query = result_query.exclude(**kwargs)
    elif not filtertype2 == None:
        kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
        rank_query = result_query.filter(**kwargs)
        withoutrank_query = result_query.exclude(**kwargs)
    else:
        rank_query = result_query
        withoutrank_query = Result.objects.none()
    
    # Bei Wertungen auf Disziplinenbasis wird - sofern es keine Mixed Disziplin ist - nur zwischen 'männlich' und 'weiblich' unterschieden
    if scoringtype == 'DIS':
        if not discipline.mixedgender:
            printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
            sorted_results = rank_results(rank_query.filter(start__person__gender='f'), printcomp, scoringcat.usequali)
            if showexternal:
                sorted_results += list(withoutrank_query.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
            printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
            sorted_results += rank_results(rank_query.filter(start__person__gender='m'), printcomp, scoringcat.usequali)
            if showexternal:
                sorted_results += list(withoutrank_query.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
        else:
            printcomp = discipline.name + ' ' + scoringcat.fullname
            sorted_results = rank_results(rank_query, printcomp, scoringcat.usequali)
            if showexternal:
                sorted_results += list(withoutrank_query.order_by(F('value').asc(nulls_last=True)))
    # Bei Wertungen auf Wettbewerbsbasis wird zwischen allen Wettbewerben unterschieden
    elif scoringtype == 'COM':
        competitions = Competition.objects.filter(event__id=event.pk, discipline=discipline, ismerged=False).order_by('gender', 'maxage')
        sorted_results = list(Result.objects.none())
        for competition in competitions:
            sorted_results += rank_results(rank_query.filter(start__competition=competition), discipline.name + ' ' + competition.fullname, scoringcat.usequali)
            if showexternal:
                sorted_results += list(withoutrank_query.filter(start__competition=competition).order_by(F('value').asc(nulls_last=True)))
    else:
        sorted_results = Result.objects.none()
        error += 'Die gewählte Wertung kann aktuell noch nicht berechnet werden!'

    # Rendered
    html_string = render_to_string('vunireg/pdf.html', {'result_list':sorted_results})
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    result = html.write_pdf()

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())

    return response
    
    
# Funktion, die das Alter eines Teilnehmers an einem bestimmten Tag (dateforage) zurückgibt
def calculate_age_at(born, dateforage):
    age = dateforage.year - born.year - ((dateforage.month, dateforage.day) < (born.month, born.day))
    return age

# Funktion, die automatisch jede Meldung einem gültigen AK-Wettbewerb des Events zuordnet
def match_entrys(event_id):
    event = get_object_or_404(Event, pk=event_id)
    #### T0DO ###
    # Dynamisch übergeben des Dateforage
    dateforage = event.startdate
    ### ENDTODO ### 
    # Alle AK-Wettbewerbe (Round = 1) des Events, die nicht zusammengelegt wurden, müssen berücksichtigt werden
    competition_query = Competition.objects.filter(event__id = event.pk, round = 1, ismerged = False).order_by('maxage')
    # Alle Meldungen des Events berücksichtigen, abzüglich der Staffelmeldungen, die von einer Person kommen und nicht zu einem Team gehören
    entry_query = Entry.objects.filter(event__id=event.pk)
    entry_query = Entry.objects.exclude(discipline__isgroupdisc__gt=1, person__isnull=False)
    # Anzahl an Meldungen für einen Wettbewerb auf Null setzten
    for competition in competition_query:
        competition.entrycount = 0
        competition.save()
    # Ermitteln der Anzahl an Meldungen für einen Wettbewerb anhand der aktuellen Meldungen
    entrycount = 0
    for competition in competition_query:
        temp_discipline = competition.discipline
        temp_maxage = competition.maxage
        temp_gender = competition.gender
        temp_event = competition.event
        tempentry_query = entry_query.filter(event=temp_event, discipline=temp_discipline)
        for tempentry in tempentry_query:
            # Bei mixed Disziplinen (m&w) muss nur das Alter für die Zuordnung berücksichtigt werden
            if (temp_discipline.mixedgender == True):
                entryage = tempentry.team.ageforcomp
                if (entryage <= temp_maxage):
                    # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
                    if not ((temp_maxage <= 10) and (tempentry.person.otheragegroup == True)):
                        tempentry.competition = competition
                        tempentry.save()
                        competition.entrycount += 1
                        competition.save()
                        entrycount += 1
                        entry_query = entry_query.exclude(id=tempentry.id)
            # Bei allen anderen Disziplinen muss das Alter & Geschlecht für die Zuordnung berücksichtigt werden
            else:
                entryage = calculate_age_at(tempentry.person.dateofbirth, dateforage)
                #entryage = dateforage - tempentry.person.dateofbirth).days / 365.2425
                entrygender = tempentry.person.gender
                if (entryage <= temp_maxage) and (entrygender == temp_gender):
                    # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
                    if not ((temp_maxage <= 10) and (tempentry.person.otheragegroup == True)):
                        tempentry.competition = competition
                        tempentry.save()
                        competition.entrycount += 1
                        competition.save()
                        entrycount += 1
                        entry_query = entry_query.exclude(id=tempentry.id)
            
    return

# Funktion, die das Zusammenlegen rückgänig macht
def unmegre_all(event_id):
    event = get_object_or_404(Event, pk=event_id)
    competition_list = Competition.objects.filter(event__id=event.pk).order_by('maxage')
    for competition in competition_list:
        competition.ismerged = False
        competition.save()
        for entry in competition.entry_set.all():
            entry.competition = None
            entry.save()
    match_entrys(event.pk)
    return

# Funktion, die automatisch AK-Wettbewerbe zusammenlegt - IUF konform - alt: Entspricht merge_competitions(event_id, fromage=0, toage=100)
def merge_competitionsIUF(event_id):
    event = get_object_or_404(Event, pk=event_id)
    # Ermitteln, wie viele Meldungen es für die einzelnen Wettbewerbe gibt
    match_entrys(event.pk)
    # Ermitteln welche Disziplinen es bei der Veranstaltung gibt
    disciplinequery = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
    # Für jede Disziplin ermitteln welche Wettbewerbe zusammengelegt werden müssen
    for selected_disc in disciplinequery:
        i = 0
        while i < 3:
            # Auswahl des Geschlechtes
            if i == 0:
                temp_gender = ''
            if i == 1:
                temp_gender = 'f'
            if i == 2:
                temp_gender = 'm'
            i +=1
            # Ermitteln, welche der betreffenden AK-Wettbewerbe zusammengelegt werden müssen
            ### To Do
            ### Mindes Teilnehmerzahl dynamisch anpassbar machen (z.B. bei weniger als 50 Teilnehmern)
            ### To Do
            merge_compquery = Competition.objects.filter(event__id=event.pk, round = 1, discipline=selected_disc, entrycount__lt=6, gender=temp_gender, ismerged=False).order_by('entrycount')
            end = 0
            while merge_compquery.count()>0:
                if end == 20:
                    break
                print(selected_disc.name + ' - ' + temp_gender)
                print(merge_compquery)
                tempquery = merge_compquery.filter(entrycount=merge_compquery.first().entrycount)
                print( 'Minimale Anzahl an Meldungen: ' + str(merge_compquery.first().entrycount) )
                print(tempquery)
                #Ermitteln, mit welchem Wettbewerb begonnen werden muss und welchem Wettbewerb zusammengelegt werden muss
                mergewith = ''
                ismerged = tempquery.first()
                mergedwith = tempquery.first()
                mergeentrycount = 99
                for merge_comp in tempquery:
                    temp_maxage = merge_comp.maxage
                    print(merge_comp.fullname + ' - Max Age: ' + str(temp_maxage) )
                    c_under = Competition.objects.filter(event__id=event.pk, round = 1, discipline=selected_disc, gender=temp_gender, maxage__lt=temp_maxage, ismerged=False).order_by('-maxage')
                    try:
                        c_under[0]
                        # Es existiert ein Wettbewerb mit geringerem Maximalalter und einer geringeren Teilnehmerzahl als dem aktuellen Minimum
                        if c_under[0].entrycount < mergeentrycount:
                            mergeentrycount = c_under[0].entrycount
                            mergewith = 'under'
                            ismerged = c_under[0]
                            mergedwith = merge_comp
                        print('Wettbewerb mit nächst geringerem Maximalalter: ' + c_under[0].discipline.name + ' ' + c_under[0].fullname + ' - Die Zahl der Anmeldungen beträgt hier: ' + str(c_under[0].entrycount) )
                    except IndexError:
                        # Es existiert kein Wettbewerb mit geringerem Maximalalter
                        print('Kein Wettbewerb mit geringerem Maximalalter gefunden!')
                    c_upper = Competition.objects.filter(event__id=event.pk, round = 1, discipline=selected_disc, gender=temp_gender, maxage__gt=temp_maxage, ismerged=False).order_by('maxage')
                    try:
                        c_upper[0]
                        # Es existiert ein Wettbewerb mit höherem Maximalalter und einer geringeren Teilnehmerzahl als dem aktuellen Minimum
                        if c_upper[0].entrycount < mergeentrycount:
                            mergeentrycount = c_upper[0].entrycount
                            mergewith = 'upper'
                            ismerged = merge_comp
                            mergedwith = c_upper[0]
                        print('Wettbewerb mit nächst höherem Maximalalter: ' + c_upper[0].discipline.name + ' ' + c_upper[0].fullname + ' - Die Zahl der Anmeldungen beträgt hier: ' + str(c_upper[0].entrycount) )
                    except IndexError:
                        # Es existiert kein Wettbewerb mit höherem Maximalalter
                        print('Kein Wettbewerb mit höherem Maximalalter gefunden!')
                # Wettbewerb zusammengelegen
                if not ismerged == mergedwith:
                    print('Der Wettbewerb: ' + ismerged.discipline.name + ' ' + ismerged.fullname + ' wird mit dem Wettbewerb: ' + mergedwith.discipline.name + ' ' + mergedwith.fullname + ' zusammengelegt.')
                    ismerged.ismerged = True
                    ismerged.save()
                    print(ismerged.ismerged)
                    mergedwith.entrycount += ismerged.entrycount
                    mergedwith.save()
                    print( str(mergedwith.entrycount) )
                    print( ismerged.entry_set )
                    for entry in ismerged.entry_set.all():
                        entry.competition = mergedwith
                        entry.save()
                else:
                    print('Kein Wettbewerb zum Zusammenlegen gefunden!')
                    break
                end +=1
                # Update von merge_compquery - ggf. sind weitere Zusammenlegungen nicht notwendig
                merge_compquery = Competition.objects.filter(event__id=event.pk, round = 1, discipline=selected_disc, entrycount__lt=6, gender=temp_gender, ismerged=False).order_by('entrycount')
    return

# Funktion, die automatisch AK-Wettbewerbe zusammenlegt - BDR konform (Altersgrenzen können vorgegeben werden)
def merge_competitions(event_id, fromage, toage):
    event = get_object_or_404(Event, pk=event_id)
    # Ermitteln, wie viele Meldungen es für die einzelnen Wettbewerbe gibt
    match_entrys(event.pk)
    # Ermitteln welche Disziplinen es bei der Veranstaltung gibt
    disciplinequery = Discipline.objects.filter(competition__in = Competition.objects.filter(event__id = event.pk, round = 1)).distinct()
    # Für jede Disziplin ermitteln welche Wettbewerbe zusammengelegt werden müssen
    for selected_disc in disciplinequery:
        i = 0
        while i < 3:
            # Auswahl des Geschlechtes
            if i == 0:
                temp_gender = ''
            if i == 1:
                temp_gender = 'f'
            if i == 2:
                temp_gender = 'm'
            i +=1
            # Ermitteln, welche der betreffenden Wettbewerbe zusammengelegt werden müssen
            ### To Do
            ### Mindes Teilnehmerzahl dynamisch anpassbar machen (z.B. bei weniger als 50 Teilnehmern)
            minentrys = 6
            ### To Do
            merge_compquery = Competition.objects.filter(event__id = event.pk, round = 1, discipline = selected_disc, entrycount__lt = minentrys, maxage__range = (fromage, toage), gender = temp_gender, ismerged = False).order_by('entrycount')
            end = 0
            # Solange Zusammenlegen, bis keine Wettbewerbe mehr übrig sind, die zusammengelegt werden müssen
            while merge_compquery.count() > 0:
                if end == 20:
                    break
                # Alle Wettbewerbe auswählen, die gleich wenig Meldungen haben, wie der Wettbewerb mit den aktuell wenigsten Meldungen
                tempquery = merge_compquery.filter(entrycount = merge_compquery.first().entrycount)
                #Ermitteln, mit welchem Wettbewerb begonnen werden muss und mit welchem Wettbewerb zusammengelegt werden muss
                mergewith = ''
                ismerged = tempquery.first()
                mergedwith = tempquery.first()
                mergeentrycount = 99
                for merge_comp in tempquery:
                    temp_maxage = merge_comp.maxage
                    tomerge_compquery = Competition.objects.filter(event__id = event.pk, round = 1, discipline = selected_disc, gender = temp_gender, maxage__range = (fromage, toage), ismerged = False).order_by('-maxage')
                    c_under = tomerge_compquery.filter(maxage__lt = temp_maxage).order_by('-maxage')
                    try:
                        c_under[0]
                        # Es existiert ein Wettbewerb mit geringerem Maximalalter und einer geringeren Teilnehmerzahl als dem aktuellen Minimum
                        if c_under[0].entrycount < mergeentrycount:
                            mergeentrycount = c_under[0].entrycount
                            mergewith = 'under'
                            ismerged = c_under[0]
                            mergedwith = merge_comp
                        print('Wettbewerb mit nächst geringerem Maximalalter: ' + c_under[0].discipline.name + ' ' + c_under[0].fullname + ' - Die Zahl der Anmeldungen beträgt hier: ' + str(c_under[0].entrycount) )
                    except IndexError:
                        # Es existiert kein Wettbewerb mit geringerem Maximalalter
                        print('Kein Wettbewerb mit geringerem Maximalalter gefunden!')
                    c_upper = tomerge_compquery.filter(maxage__gt = temp_maxage).order_by('maxage')
                    try:
                        c_upper[0]
                        # Es existiert ein Wettbewerb mit höherem Maximalalter und einer geringeren Teilnehmerzahl als dem aktuellen Minimum
                        if c_upper[0].entrycount < mergeentrycount:
                            mergeentrycount = c_upper[0].entrycount
                            mergewith = 'upper'
                            ismerged = merge_comp
                            mergedwith = c_upper[0]
                        print('Wettbewerb mit nächst höherem Maximalalter: ' + c_upper[0].discipline.name + ' ' + c_upper[0].fullname + ' - Die Zahl der Anmeldungen beträgt hier: ' + str(c_upper[0].entrycount) )
                    except IndexError:
                        # Es existiert kein Wettbewerb mit höherem Maximalalter
                        print('Kein Wettbewerb mit höherem Maximalalter gefunden!')
                # Wettbewerb zusammengelegen
                if not ismerged == mergedwith:
                    print('Der Wettbewerb: ' + ismerged.discipline.name + ' ' + ismerged.fullname + ' wird mit dem Wettbewerb: ' + mergedwith.discipline.name + ' ' + mergedwith.fullname + ' zusammengelegt.')
                    ismerged.ismerged = True
                    ismerged.save()
                    mergedwith.entrycount += ismerged.entrycount
                    mergedwith.save()
                    for entry in ismerged.entry_set.all():
                        entry.competition = mergedwith
                        entry.save()
                else:
                    print('Kein Wettbewerb zum Zusammenlegen gefunden!')
                    break
                end +=1
                # Update von merge_compquery - ggf. sind weitere Zusammenlegungen nicht notwendig
                merge_compquery = Competition.objects.filter(event__id = event.pk, round = 1, discipline = selected_disc, entrycount__lt = minentrys, maxage__range = (fromage, toage), gender = temp_gender, ismerged = False).order_by('entrycount')
    return
    
# Funktion, die einzelne Wettbewerbe löscht
def del_singlecompetition(request, event_id, competition_id):
    event = get_object_or_404(Event, pk=event_id)
    competition = get_object_or_404(Competition, pk=competition_id)
    response = ''
    error = ''
    if competition.entry_set.all() and competition.start_set.all():
        error += 'Der Wettbewerb - ' + competition.discipline.name + ' ' + competition.fullname + ' ' + competition.gender + ' - kann nicht gelöscht werden, da ihm bereits Meldungen und Starts zugeordnet sind!' + '</br>'
    elif competition.entry_set.all():
        error += 'Der Wettbewerb - ' + competition.discipline.name + ' ' + competition.fullname + ' ' + competition.gender + ' - kann nicht gelöscht werden, da ihm bereits Meldungen zugeordnet sind!' + '</br>'
    elif competition.start_set.all():
        error += 'Der Wettbewerb - ' + competition.discipline.name + ' ' + competition.fullname + ' ' + competition.gender + ' - kann nicht gelöscht werden, da ihm bereits Starts zugeordnet sind!' + '</br>'
    else:
        competition.delete()
        response += 'Der Wettbewerb ' + competition.fullname + ' wurde gelöscht.' + '</br>'
    competition_list = Competition.objects.filter(event__id=event.pk).order_by('discipline', 'gender', 'maxage')
    competition_filter = CompetitionFilter(request.GET, queryset = competition_list)
    # Ausgabe
    content = {
        'filter':competition_filter, 'event':event, 'response':response, 'error':error,
    }
    return render(request, "vunireg/competition_list.html", content)
#    return redirect("vunireg:competition_list", event_id)

# Funktion, die einzelne Wettbewerbe zusammenlegt
### TODO ###
### Funktion so in die view Funktion der Übersichtsseite integrieren, dass die URL gleich bleibt
def merge_singlecompetition(request, event_id, competition_id):
    event = get_object_or_404(Event, pk=event_id)
    competition = get_object_or_404(Competition, pk=competition_id)
    response = ''
    error = ''
    if competition.entry_set.all() and competition.start_set.all():
        error += 'Der Wettbewerb - ' + competition.discipline.name + ' ' + competition.fullname + ' ' + competition.gender + ' - kann nicht zusammengelegt werden, da ihm bereits Meldungen und Starts zugeordnet sind!' + '</br>'
    elif competition.entry_set.all():
        error += 'Der Wettbewerb - ' + competition.discipline.name + ' ' + competition.fullname + ' ' + competition.gender + ' - kann nicht zusammengelegt werden, da ihm bereits Meldungen zugeordnet sind!' + '</br>'
    elif competition.start_set.all():
        error += 'Der Wettbewerb - ' + competition.discipline.name + ' ' + competition.fullname + ' ' + competition.gender + ' - kann nicht zusammengelegt werden, da ihm bereits Starts zugeordnet sind!' + '</br>'
    else:
        competition.ismerged = True
        competition.save()
        response += 'Der Wettbewerb ' + competition.fullname + ' wurde zusammengelegt.' + '</br>'
    competition_list = Competition.objects.filter(event__id=event.pk).order_by('discipline', 'gender', 'maxage')
    competition_filter = CompetitionFilter(request.GET, queryset = competition_list)
    # Ausgabe
    content = {
        'filter':competition_filter, 'event':event, 'response':response, 'error':error,
    }
    return render(request, "vunireg/competition_list.html", content)
#    return redirect("vunireg:competition_list", event_id)
### ENDTODO ###
