# Generated by Django 3.0.5 on 2020-04-02 07:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Club',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clubname', models.CharField(max_length=100)),
                ('individualstart', models.BooleanField(default=False, null=True, verbose_name='Einzelstarter')),
            ],
            options={
                'verbose_name': 'Verein',
                'verbose_name_plural': 'Vereine',
                'ordering': ('clubname', 'clubname'),
            },
        ),
        migrations.CreateModel(
            name='Discipline',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('group', models.CharField(choices=[('', ''), ('TRA', 'Track & Field'), ('FRE', 'Freestyle'), ('MUN', 'Muni'), ('ROA', 'Road'), ('URB', 'Urban'), ('TEA', 'Team')], max_length=3)),
                ('ranking', models.CharField(max_length=100)),
                ('custom1', models.CharField(blank=True, max_length=100, null=True, verbose_name='CustomField1')),
                ('custom2', models.CharField(blank=True, max_length=100, null=True, verbose_name='CustomField2')),
            ],
            options={
                'verbose_name': 'Disziplin',
                'verbose_name_plural': 'Disziplinen',
                'ordering': ('name', 'name'),
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('slug', models.SlugField(help_text='Kürzel für die URL: nur Buchstaben, Zahlen und Striche - keine Leerzeichen!', unique=True, verbose_name='Kürzel')),
                ('start', models.DateField()),
                ('end', models.DateField()),
                ('location', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('host', models.CharField(max_length=100, verbose_name='Ausrichter')),
                ('contact', models.CharField(max_length=100, verbose_name='Ansprechpartner')),
                ('contactmail', models.EmailField(max_length=254, verbose_name='E-Mail-Kontakt')),
            ],
            options={
                'verbose_name': 'Veranstaltung',
                'verbose_name_plural': 'Veranstaltungen',
                'ordering': ('start', 'name'),
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('forename', models.CharField(max_length=100, verbose_name='Vorname')),
                ('surname', models.CharField(max_length=100, verbose_name='Nachname')),
                ('gender', models.CharField(choices=[('', ''), ('f', 'weiblich'), ('m', 'männlich')], max_length=1, verbose_name='Geschlecht')),
                ('dateofbirth', models.DateField()),
                ('iscontact', models.BooleanField(default=False, verbose_name='Ist Kontaktperson')),
                ('custom1', models.CharField(blank=True, max_length=100, null=True, verbose_name='CustomField1')),
                ('custom2', models.CharField(blank=True, max_length=100, null=True, verbose_name='CustomField2')),
                ('club', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='vunireg.Club')),
            ],
            options={
                'verbose_name': 'Teilnehmer',
                'verbose_name_plural': 'Teilnehmer',
                'ordering': ('club', 'surname'),
            },
        ),
        migrations.CreateModel(
            name='PersonDetails',
            fields=[
                ('person', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='vunireg.Person')),
                ('mail', models.EmailField(max_length=254, verbose_name='E-Mail-Kontakt')),
                ('phone1', models.CharField(max_length=20)),
                ('phone2', models.CharField(max_length=20)),
                ('adress', models.CharField(max_length=100)),
                ('zipcode', models.CharField(max_length=20, verbose_name='PLZ')),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100, verbose_name='Land')),
            ],
            options={
                'verbose_name': 'Persönliche Daten',
                'verbose_name_plural': 'Persönliche Daten',
                'ordering': ('person', 'person'),
            },
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(blank=True, max_length=100, null=True)),
                ('discipline', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vunireg.Discipline')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vunireg.Event')),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vunireg.Person')),
            ],
            options={
                'verbose_name': 'Meldung',
                'verbose_name_plural': 'Meldungen',
                'ordering': ('person', 'discipline'),
            },
        ),
        migrations.CreateModel(
            name='Competition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20, verbose_name='Bezeichnung')),
                ('agefrom', models.PositiveIntegerField(default=0, verbose_name='Alter von')),
                ('ageto', models.PositiveIntegerField(default=100, verbose_name='Alter bis')),
                ('gender', models.CharField(choices=[('', ''), ('f', 'weiblich'), ('m', 'männlich')], max_length=1, verbose_name='Geschlecht')),
                ('discipline', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vunireg.Discipline')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='vunireg.Event')),
            ],
            options={
                'verbose_name': 'Wettkampf',
                'verbose_name_plural': 'Wettkämpfe',
                'ordering': ('discipline', 'name'),
            },
        ),
        migrations.AddField(
            model_name='club',
            name='contactperson',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='vunireg.Person'),
        ),
        migrations.AddConstraint(
            model_name='person',
            constraint=models.UniqueConstraint(fields=('forename', 'surname', 'dateofbirth'), name='unique_person'),
        ),
        migrations.AddConstraint(
            model_name='entry',
            constraint=models.UniqueConstraint(fields=('person', 'discipline'), name='unique_entry'),
        ),
    ]
