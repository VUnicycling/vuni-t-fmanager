from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
import datetime
from datetime import date
import csv
import types
from django.db.models import F, Func
from django.db.models import FloatField
from django.db.models.functions import Cast

from django.template.loader import render_to_string
from django.conf import settings
from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
import tempfile

from vunireg.models import Event, Person, Club, Entry, Discipline, Agegroup, Competition, Start, Scoringcategory, Qualification, Result
from .filters import PersonFilter, ClubFilter, EntryFilter, CompetitionFilter
from .forms import EventEditForm, PersonEditForm, EntryEditInlineForm, ScoringcatEditForm, QualificationEditForm, ResultEditInlineFormset


# Funktion, die das Alter eines Teilnehmers an einem bestimmten Tag (dateforage) zurückgibt
def calculate_age_at(born, dateforage):
    age = dateforage.year - born.year - ((dateforage.month, dateforage.day) < (born.month, born.day))
    print(age)
    return age

# Funktion, zur Ausgabe der Übersichtsseite
def execution(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    scoringcat_query = Scoringcategory.objects.filter(event__id=event.pk)
    competition_query = Competition.objects.filter(event__id=event.pk, ismerged=False)
    competition_query = competition_query.exclude(start__isnull=True).distinct().order_by('discipline', 'gender', 'maxage')
    discipline_query = Discipline.objects.filter(competition__in=competition_query).distinct()
    club_query = Club.objects.filter(person__entry__event__id=event.pk).distinct()
    # Abfrage, für welchen Wettbewerb Ergebnisse eingegeben werden sollen
    if request.method == "POST":
        # Wenn Ergebnisse eingeben werden sollen, Ergebniseingabe öffnen
        if 'edit_results' in request.POST:
            resultcomp = request.POST.get('comp')
            competition = get_object_or_404(Competition, pk=resultcomp)
            return redirect('vunireg:execution_edit_result', event_id=event_id, competition_id=competition.pk)
        # Wenn Ergebnisse ausgegeben werden sollen, entsprechende Ausgabe öffnen
        if 'show_results' in request.POST:
            resultscoringcat = request.POST.get('scoringcat')
            scoringcat = get_object_or_404(Scoringcategory, pk=resultscoringcat)
            resultdiscipline = request.POST.get('discipline')
            discipline = get_object_or_404(Discipline, pk=resultdiscipline)
            return redirect('vunireg:execution_show_result', event_id=event_id, scoringcat_id=scoringcat.pk, discipline_id=discipline.pk)
        # Wenn Ergebnisse gedruckt werden sollen, entsprechende Ausgabe öffnen
        if 'print_results' in request.POST:
            resultscoringcat = request.POST.get('scoringcat')
            scoringcat = get_object_or_404(Scoringcategory, pk=resultscoringcat)
            resultdiscipline = request.POST.get('discipline')
            discipline = get_object_or_404(Discipline, pk=resultdiscipline)
            return redirect('vunireg:print_resultlist', event_id=event.pk, scoringcat_id=scoringcat.pk, discipline_id=discipline.pk)
        # Wenn Sieger-Urkunden gedruckt werden sollen, entsprechende Ausgabe öffnen
        if 'print_cert_pdf' in request.POST:
            resultscoringcat = request.POST.get('scoringcat')
            scoringcat = get_object_or_404(Scoringcategory, pk=resultscoringcat)
            resultdiscipline = request.POST.get('discipline')
            discipline = get_object_or_404(Discipline, pk=resultdiscipline)
        # Wenn Teilnehmer-Urkunden gedruckt werden sollen, entsprechende öffnen
        if 'print_compcert_pdf' in request.POST:
            printclub = request.POST.get('club')
            club = get_object_or_404(Club, pk=printclub)
            return redirect('vunireg:print_compcert_pdf', event_id=event.pk, club_id=club.pk)
    content = {
        'competition_list':competition_query, 'scoringcat_list':scoringcat_query, 'discipline_list':discipline_query, 'club_list':club_query, 'response':response, 'error':error, 'event':event,
    }
    return render(request, "vunireg/results_overview.html", content)

# Funktion, zur Ergebniseingabe
def execution_edit_result(request, event_id, competition_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    competition_query = Competition.objects.filter(event__id=event.pk, ismerged=False)
    competition_query = competition_query.exclude(start__isnull=True).distinct().order_by('discipline', 'gender', 'maxage')
    # Auswerten der Benutzereingabe
    if request.method == "POST":
        # Wenn ein anderer Wettbewerb ausgewählt wird, Wettbewerb wechseln
        if 'edit_results' in request.POST:
            resultcomp = request.POST.get('comp')
            competition = get_object_or_404(Competition, pk=resultcomp)
            return redirect('vunireg:execution_edit_result', event_id=event_id, competition_id=competition.pk)
    if not competition_id == 'None':
        competition = get_object_or_404(Competition, pk=competition_id)
        # Starts ermitteln, die in diesem Wettbewerb vorhanden sind
        starts_list = Start.objects.filter(competition__id=competition.pk).order_by('heat', 'lane')
        print(starts_list)
        results_list = Result.objects.none()
        for start in starts_list:
            if start.result_set.exists():
                # Es existiert bereits ein Ergebnis zu diesem Start - zeige dieses Ergebnis
                results_list |= start.result_set.all()
                print('Bereits ein Ergebnis vorhanden')
            else:
                # Es wird ein neues Ergebnis angelegt
                result = Result(event=event, start=start)
                result.save()
                results_list |= Result.objects.filter(id=result.pk)
        formset = ResultEditInlineFormset(queryset=results_list.order_by('start__heat', 'start__lane'))
        if request.method == "POST":
            # Wenn ein anderer Wettbewerb ausgewählt wird, Wettbewerb wechseln
            if 'edit_results' in request.POST:
                resultcomp = request.POST.get('comp')
                competition = get_object_or_404(Competition, pk=resultcomp)
                return redirect('vunireg:execution_edit_result', event_id=event_id, competition_id=competition.pk)
            # Wenn Ergebnisse eingetragen wurden und gesichert werden sollen, Ergebnisse sichern
            if 'save_results' in request.POST:
                formset = ResultEditInlineFormset(request.POST)
                if formset.has_changed():
                    if formset.is_valid():
                        new_instances = formset.save(commit=False)
                        for new_instance in new_instances:
                            new_instance.save()
                            response += 'Das folgende Ergebnis wurde hinzugefügt oder bearbeitet: ' + new_instance.start.person.forename + ' ' + new_instance.start.person.surname + ' ' + new_instance.start.entry.competition.discipline.name  + '<br />'
                        response += 'Änderungen gespeichert'  + '<br />'
                        print(response)
                    else:
                        error += 'Das Formular konnte nicht validiert werden!'  + '<br />'
        # Ausgabe
        content = {
            'formset':formset, 'competition_list':competition_query, 'starts_list':starts_list, 'competition_id':competition_id, 'event':event, 'response':response, 'error':error
        }
    else:
        # Ausgabe
        content = {
            'competition_list':competition_query, 'competition_id':competition_id, 'event':event, 'response':response, 'error':error
        }
    return render(request, "vunireg/results_enter.html", content)

# Importieren von Ergebnissen als CSV Datei
def import_result_csv(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    text = ''
    if request.method == "POST":
        csvfile = request.POST.get('csvfile', False)
        format = request.POST.get('format')
        if format == 'FinishLynx':
            response = 'FinishLynx Format ausgewählt!' + '</br>'
            ### START: Import von FinishLynx CSV Dateien ###
            if csvfile:
                n = 0
                for row in csvfile.splitlines():
                    data = row.split(',')
                    txt = ''
                    n += 1
                    datacount = len(data)
                    # Bei den Dateien gibt es immer eine Reihe mit allgemeinen Daten für den Lauf, die eine Länge von 11 hat
                    if datacount == 11:
                        text += ','.join(data) + '\r\n'
                        ### Bedeutung der FinishLynx Daten ###
                        imp_discipline = data[0]
                        imp_competitionround = data[1]
                        imp_heat = data[2]
                        imp_competition = data[3]
                        imp_windvalue = data[4]
                        imp_windunit = data[5]
                        # Einträge 6 bis 9 sind unbekannt
                        imp_starttime = data[10]
                        ### Bedeutung der FinishLynx Daten ###
                
                        # überprüfen, ob Disziplin vorhanden
                        try:
                            discipline = Discipline.objects.get(name__iexact=imp_discipline)
                        except Discipline.DoesNotExist:
                            try:
                                discipline = Discipline.objects.get(code__iexact=imp_discipline)
                            except Discipline.DoesNotExist:
                                error += 'ERROR: Die Disziplin ' + imp_discipline + ' ist nicht bekannt!' + '</br>'
                                text += 'ERROR: Die Disziplin ' + imp_discipline + ' ist nicht bekannt!\r\n'
                                break
                        # überprüfen, ob Wettbewerb vorhanden
                        competition_list = Competition.objects.filter(discipline=discipline)
                        print(competition_list)
                        try:
                            competition = Competition.objects.get(discipline=discipline, fullname__iexact=imp_competition)
                        except Competition.DoesNotExist:
                            error += 'ERROR: Der Wettbewerb ' + imp_competition + ' ist nicht bekannt!' + '</br>'
                            text += 'ERROR: Der Wettbewerb ' + imp_competition + ' ist nicht bekannt!\r\n'
                            break
                        wind = imp_windvalue + ',' + imp_windunit
                        print(discipline.name + ' ' + competition.fullname + ' - Runde: ' + imp_competitionround + ' - Lauf: ' + imp_heat + ' - ' + wind + ' - ' + imp_starttime)
                    # Die Reihen für die einzelnen Ergebnisse eines Laufes haben eine Länge vom 17
                    elif datacount == 19:
                        text += ','.join(data) + '\r\n'
                        ### Bedeutung der FinishLynx Daten ###
                        imp_place = data[0]
                        imp_startnr = data[1]
                        imp_lane = data[2]
                        imp_surname = data[3]
                        imp_forename = data[4]
                        imp_club = data[5]
                        imp_result = data[6]
                        # imp_license = data[7]
                        imp_timediff = data[8]
                        # Einträge 9 und 10 sind unbekannt
                        imp_starttime = data[11]
                        imp_user1 = data[12]
                        imp_user2 = data[13]
                        imp_user3 = data[14]
                        # Einträge 15 bis 18 sind unbekannt
                        ### Bedeutung der FinishLynx Daten ###
                
                        # Überprüfen, ob ein entsprechender Teilnehmer mit der passenden Startnummer vorhanden ist
                        try:
                            person = Person.objects.get(startnr=imp_startnr)
                            if not((person.surname==imp_surname) and (person.forename==imp_forename)):                        
                                error += 'Der Teilnehmer ' + person.forename + ' ' + person.surname + ' hat in der Datenbank die Startnummer ' + str(person.startnr) + '!' + '</br>'
                                text += 'Der Teilnehmer ' + person.forename + ' ' + person.surname + ' hat in der Datenbank die Startnummer ' + str(person.startnr) + '!\r\n'
                                continue
                        except Person.DoesNotExist:
                            error += 'In der Datenbank gibt es keinen Teilnehmer mit der Startnummer ' + imp_startnr + '!' + '</br>'
                            text += 'In der Datenbank gibt es keinen Teilnehmer mit der Startnummer ' + imp_startnr + '!\r\n'
                            try:
                                person = Person.objects.get(surname__iexact=imp_surname, forename__iexact=imp_forename)
                                error += 'Der Teilnehmer ' + imp_forename + ' ' + imp_surname + ' hat in der Datenbank die Startnummer ' + str(person.startnr) + '!' + '</br>'
                                text += 'Der Teilnehmer ' + imp_forename + ' ' + imp_surname + ' hat in der Datenbank die Startnummer ' + str(person.startnr) + '!\r\n'
                                continue
                            except Person.DoesNotExist:
                                error += 'In der Datenbank gibt es keinen Teilnehmer ' + imp_forename + ' ' + imp_surname + '!' + '</br>'
                                text += 'In der Datenbank gibt es keinen Teilnehmer ' + imp_forename + ' ' + imp_surname + '!\r\n'
                                continue
                        # Überprüfen, ob ein entsprechender Start vorhanden ist
                        try:
                            start = Start.objects.get(person=person, competition=competition, heat=imp_heat, lane=imp_lane)
                            # Überprüfen, ob bereits ein entsprechendes Ergebnis vorhanden ist
                            try:
                                result = Result.objects.get(start=start)
                                result.value = imp_result
                                result.diff = imp_timediff
                                result.custom1 = wind
                                result.save()
                                response += 'Ergebnis aktualisiert und in der Datenbank gespeichert!' + '</br>'
                            except Result.DoesNotExist:
                                result = Result(start=start, value=imp_result, diff=imp_timediff, custom1=wind)
                                result.save()
                                response = 'Ergebnis neu angelegt und in der Datenbank gespeichert!' + '</br>'
                        except Start.DoesNotExist:
                            error += 'In der Datenbank gibt es keinen Start dem das Ergebnis der Reihe ' + str(n) + '  zugeordnet werden kann!' + '</br>'
                            text += 'In der Datenbank gibt es keinen Start dem dieses Ergebnis zugeordnet werden kann!\r\n'
                    else:
                        error += 'ERROR: Die in der Reihe ' + str(n) + ' übergebenen Daten haben nicht die richtige Länge für das LynxFinish Format! ' + str(datacount) +'</br>'
                        text += 'ERROR: Die in der Reihe übergebenen Daten haben nicht die richtige Länge für das LynxFinish Format!\r\n'
            ### END: Import von FinishLynx CSV Dateien ###
        elif format == 'VUni':
            response = 'Vocke Unicycling Virtual Competition Format ausgewählt!' + '</br>'
            ### START: Import von VUni CSV Dateien ###
            if csvfile:
                n = 0
                for row in csvfile.splitlines():
                    data = row.split(',')
                    txt = ''
                    n += 1
                    datacount = len(data)
                    # Jede Reihe enthält alle Ergebnisse eines Teilnehmers
                    if datacount == 23:
                        text += ','.join(data) + '\r\n'
                        ### Bedeutung der VUni Virtual Competition Daten ###
                        imp_surname = data[0]
                        imp_forename = data[1]
                        imp_club = data[2]
                        imp_dateofbirth = data[3]
                        imp_gender = data[4]
                        # Einträge 6 bis 23 sind die Disziplinen und zugehörigen Ergebnisse
                        ### Bedeutung der VUni Virtual Competition Daten ###
                        
                        ### Importieren der übergebenen Daten in die Datenbank ###  
                        # Überprüfen, ob Verein vorhanden
                        try:
                            v = Club.objects.get(clubname__iexact=data[2])
                            txt += 'Verein vorhanden - '
                        except Club.DoesNotExist:
                            txt += 'Verein nicht vorhanden - '
                            if data[2] == '':
                                v = Club(clubname=('vereinslos_' + data[0]), individualstart=True)
                                v.save()
                            else:
                                v = Club(clubname=data[2], individualstart=False)
                                v.save()  
                        # überprüfen, ob Teilnehmer vorhanden
                        try:
                            t = Person.objects.get(surname__iexact=data[0], forename__iexact=data[1], dateofbirth=data[3])
                            temp_person = t
                            txt += 'Teilnehmer vorhanden - '
                        except Person.DoesNotExist:
                            txt += 'Teilnehmer nicht vorhanden - '
                            if data[4] == 'w':
                                data[4] = 'f'
                            t = Person(surname=data[0], forename=data[1], dateofbirth=data[3], gender=data[4], club=v)
                            t.save()
                            temp_person = t
                        # Bei mehr als 5 Elementen werden zusätzlich zu den Teilnehmerdaten noch Meldungen übergeben
                        if datacount > 5:
                            i = 0
                            while i < (datacount-5):
                                i += 1
                                # überprüfen, ob Disziplin vorhanden
                                try:
                                    d = Discipline.objects.get(name__iexact=data[(4+i)])
                                    txt += data[(4+i)] + ' in DB - '
                                except Discipline.DoesNotExist:
                                    try:
                                        d = Discipline.objects.get(code__iexact=data[(4+i)])
                                        txt += data[(4+i)] + ' in DB - '
                                    except Discipline.DoesNotExist:
                                        error += 'ERROR: Die Disziplin ' + data[(4+i)] + ' ist nicht bekannt!'
                                        text += 'ERROR: Die Disziplin ' + data[(4+i)] + ' ist nicht bekannt!\r\n'
                                        break
                                # überprüfen, ob zur Disziplin ein Ergebnis vorhanden ist
                                if data[(5+i)] == '' :
                                    txt += 'Kein Ergebnis übergeben - '
                                    i += 1
                                else:
                                    txt += 'Ergebnis übergeben - '
                                    # Überprüfen, ob Meldung vorhanden ist                                    
                                    try:
                                        m = Entry.objects.get(event=event, person=temp_person, discipline=d)
                                        if not m.value == data[(5+i)]:
                                            m.value=data[(5+i)]
                                            m.save()
                                            txt += 'Meldezeit aktualisiert: ' + data[(5+i)] + ' - '
                                            response += d.name + ' Meldung aktualisiert und in der Datenbank gespeichert!' + '</br>'
                                        else:
                                            txt += 'Meldung breits vorhanden! - '
                                            response += d.name + ' Meldung bereits in der Datenbank vorhanden!' + '</br>'
                                        temp_entry = m
                                    except Entry.DoesNotExist:
                                        m = Entry(event=event, person=temp_person , discipline=d, value=data[(5+i)])
                                        m.save()
                                        txt += 'Meldung angelegt: ' + data[(5+i)] + ' - '
                                        response = d.name + ' Meldung neu angelegt und in der Datenbank gespeichert!' + '</br>'
                                        temp_entry = m
                                    # Überprüfen, ob Start vorhanden ist
                                    try:
                                        start = Start.objects.get(event=event, person=temp_person, entry=temp_entry)
                                        response += d.name + ' Start bereits in der Datenbank vorhanden!' + '</br>'
                                    except Start.DoesNotExist:
                                        # Für die Auswertung die Meldung direkt einem Wettbewerb zuordnen
                                        dateforage = event.enddate
                                        competition_list = Competition.objects.filter(event__id=event.pk, ismerged=False, discipline=d).order_by('maxage')
                                        for competition in competition_list:
                                            temp_discipline = competition.discipline
                                            temp_maxage = competition.maxage
                                            temp_gender = competition.gender
                                            temp_event = competition.event
                                            temp_dateofbirth = date.today()
                                            print(temp_dateofbirth)
                                            temp_entry = Entry.objects.get(event=event, person=temp_person, discipline=d)
                                            temp_dateofbirth = temp_entry.person.dateofbirth
                                            print('Geburtsdatum: ' + str(temp_dateofbirth))
                                            print('Date for Age Calulation: ' + str(dateforage))
                                            entryage = calculate_age_at(temp_dateofbirth, dateforage)
                                            entrygender = temp_entry.person.gender
                                            # Bei mixed Disziplinen (m&w) muss nur das Alter für die Zuordnung berücksichtigt werden
                                            if (temp_gender == ''):
                                                if (entryage <= temp_maxage):
                                                    # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
                                                    if not ((temp_maxage <= 10) and (temp_entry.person.otheragegroup == True)):
                                                        temp_entry.competition = competition
                                                        temp_entry.save()
                                                        txt += 'Meldung zum Wettbewerb ' + competition.fullname + ' hinzugefügt - '
                                                        competition.entrycount += 1
                                                        competition.save()
                                                        # Start anlegen
                                                        start = Start(event=event, person=t, entry=temp_entry, competition=competition, value=temp_entry.value)
                                                        start.save()
                                                        break
                                            # Bei allen anderen Disziplinen muss das Alter & Geschlecht für die Zuordnung berücksichtigt werden
                                            elif (entryage <= temp_maxage) and (entrygender == temp_gender):
                                                # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
                                                if not ((temp_maxage <= 10) and (temp_entry.person.otheragegroup == True)):
                                                    temp_entry.competition = competition
                                                    temp_entry.save()
                                                    txt += 'Meldung zum Wettbewerb ' + competition.fullname + ' hinzugefügt - '
                                                    competition.entrycount += 1
                                                    competition.save()
                                                    # Start anlegen
                                                    start = Start(event=event, person=t, entry=temp_entry, competition=competition, value=temp_entry.value)
                                                    start.save()
                                                    break
                                    # Überprüfen, ob Ergebnis vorhanden ist
                                    try:
                                        result = Result.objects.get(event=event, person=t, discipline=d)
                                        if not result.value == data[(5+i)]:
                                            result.value = data[(5+i)]
                                            result.save()
                                            response += d.name + ' Ergebnis aktualisiert und in der Datenbank gespeichert!' + '</br>'
                                        else:
                                            response += d.name + ' Ergebnis bereits in der Datenbank vorhanden!' + '</br>'
                                    except Result.DoesNotExist:
                                        result = Result(event=event, person=t, discipline=d, start=start, value=data[(5+i)])
                                        result.save()
                                        response = d.name + ' Ergebnis neu angelegt und in der Datenbank gespeichert!' + '</br>'
                                    i += 1
                    else:
                        error += 'ERROR: Die in der Reihe ' + str(n) + ' übergebenen Daten haben nicht die richtige Länge für das VUni Virtual Competition Format! ' + str(datacount) +'</br>'
                        text += 'ERROR: Die in der Reihe übergebenen Daten haben nicht die richtige Länge für das VUni Virtual Competition Format!\r\n'
                    text += txt + '\r\n'
                    text += ','.join(data) + '\r\n'
                    txt = ''
            else:
                text += txt + '\r\n'
                text += ','.join(data) + '\r\n'
            ### END: Import von VUni CSV Dateien ###
    # Ausgabe
    content = {
        'text':text, 'error':error, 'response':response, 'event':event,
    }
    return render(request, "vunireg/import_result_csv.html", content)

# Funktion, welche überprüft, ob die Mindestqualifikation für eine übergebene Disziplin, Alter und Geschlecht erfüllt ist
def check_quali(discipline, age, gender, result):
    try:
        quali = Qualification.objects.get(discipline = discipline, minage__lte = age, maxage__gte = age)
        qualivalue = quali.value
    except:
        return 1
    if(qualivalue and discipline.ranking == 'ASC'):
       if(result <= qualivalue):
           return 1
       else:
           return 0
    elif(qualivalue and discipline.ranking == 'DSC'):
       if(result >= qualivalue):
           return 1
       else:
           return 0
    else:
       return 1

# Funktion, zur Berechnung der Platzierung
def rank_results(result_query, ranking, isgroupdisc, printcomp, usequali):
    if ranking == 'ASC':
        for result in result_query:
            if ":" in result.value:
                result.num_value=float(result.value.split(':')[1])
                result.first_num_value=float(result.value.split(':')[0])
            else:
                result.num_value=float(result.value)
                result.first_num_value=0
        results = sorted(result_query, key=lambda o: (o.first_num_value, o.num_value))
        result_query = results
    else:
        results = result_query.annotate(num_value=Cast('value', FloatField())).order_by(F('num_value').desc(nulls_last=True))
    current_rank = 1
    counter = 0
    for result in results:
    ### Fälle neben einem gültigen Ergebnis berücksichtigen: (1.-5. value=None, 6. value=x)
    ### 1. WDR - Abgemeldet nicht am Start
    ### 2. DNS - Unabgemeldet nicht am Start
    ### 3. DNF - Rennen nicht beendet
    ### 4. DISQ - Disqualifiziert (+Comment)
    ### 5. WVA - Ohne gültigen Versuch
    ### --- Sonderfall --- Value muss erhalten bleiben!!!
    ### 6. QNA - Qualifikation nicht erreicht
        if result.value == None:
            if result.comment=='WDR':
                result.printsection = 'abgemeldet'
            elif result.comment=='DNS':
                result.printsection = 'nicht am Start'
            elif result.comment=='DNF':
                result.printsection = 'nicht beendet'
            elif result.comment=='DISQ':
                result.printsection = 'disqualifiziert'
            elif result.comment=='WVA':
                result.printsection = 'ohne gültigen Versuch'
            else:
                result.printsection = 'ERROR - kein gültiger Bezeichner gefunden'
            result.rank = ''
        else:
            if counter < 1:
                ### To Do ###
                ### Überprüfen, ob die Qualifikation erreicht wurde
                if(usequali):
                    if isgroupdisc == 1:
                        quali_ok = check_quali(result.start.entry.competition.discipline, result.start.person.age_at(result.start.entry.event.startdate), result.start.person.gender, result.value)
                    else:
                        quali_ok = check_quali(result.start.entry.competition.discipline, result.start.entry.team.ageforcomp, result.start.entry.competition.gender, result.value)
                else:
                    quali_ok = 1
                if (quali_ok):
                    result.rank = current_rank
                    counter += 1
                else:
                    result.rank = 'QNA'
                    result.comment = 'QNA'
                ### To Do ###
                result.printsection = 'RANKED'
            else:
                ### To Do ###
                ### Überprüfen, ob die Qualifikation erreicht wurde
                if(usequali):
                    if isgroupdisc == 1:
                        quali_ok = check_quali(result.start.entry.competition.discipline, result.start.person.age_at(result.start.entry.event.startdate), result.start.person.gender, result.value)
                    else:
                        quali_ok = check_quali(result.start.entry.competition.discipline, result.start.entry.team.ageforcomp, result.start.entry.competition.gender, result.value)
                else:
                    quali_ok = 1
                if (quali_ok):
                    if result.value == results[counter - 1].value:
                        ### To Do ###
                        ### Überprüfen, ob das Fotofinish über die Reihenfolge entscheiden kann (Plätze 1-3)
                        ### To Do ###
                        result.rank = current_rank
                        counter += 1
                    else:
                        current_rank = counter + 1
                        result.rank = current_rank
                        counter += 1
                else:
                    result.rank = 'QNA'
                    result.comment = 'QNA'
                ### To Do ###
                result.printsection = 'RANKED'
        result.printcomp = printcomp
    return list(results)
    
# Funktion, zur Ergebnisausgabe
def execution_show_result(request, event_id, scoringcat_id, discipline_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    scoringcat_list = Scoringcategory.objects.filter(event__id=event.pk)
    competition_query = Competition.objects.filter(event__id=event.pk, ismerged=False)
    competition_query = competition_query.exclude(start__isnull=True).distinct().order_by('discipline', 'gender', 'maxage')
    discipline_query = Discipline.objects.filter(competition__in=competition_query).distinct()
    if request.method == "POST":
        # Wenn die Ergebnisse als HTML Seite im Programm ausgegeben werden sollen entsprechende Wertungskategorie und Disziplin übergeben und Ergebnisse ausgeben
        if 'show_results' in request.POST:
            resultscoringcat = request.POST.get('scoringcat')
            scoringcat = get_object_or_404(Scoringcategory, pk=resultscoringcat)
            resultdiscipline = request.POST.get('discipline')
            discipline = get_object_or_404(Discipline, pk=resultdiscipline)
            return redirect('vunireg:execution_show_result', event_id=event_id, scoringcat_id=scoringcat.pk, discipline_id=discipline.pk)
        # Wenn eine Ergebnisliste als PDF gedruckt werden soll entsprechende Wertungskategorie und Disziplin übergeben und PDF drucken
        if 'print_results' in request.POST:
            resultscoringcat = request.POST.get('scoringcat')
            scoringcat = get_object_or_404(Scoringcategory, pk=resultscoringcat)
            resultdiscipline = request.POST.get('discipline')
            discipline = get_object_or_404(Discipline, pk=resultdiscipline)
            return redirect('vunireg:print_resultlist', event_id=event.pk, scoringcat_id=scoringcat.pk, discipline_id=discipline.pk)
    if not (scoringcat_id == 'None' or discipline_id == 'None'):
        scoringcat = get_object_or_404(Scoringcategory, pk=scoringcat_id)
        discipline = get_object_or_404(Discipline, pk=discipline_id)
        sorted_results = None
        # Allgemeine Werte
        ### TODO ###
    	# Dynamisch übergeben des Dateforage
        dateforage = event.enddate
        ### ENDTODO ###
        # Filter für die Wertung bestimmen
        scoringtype = scoringcat.type
        filtertype1 = scoringcat.filtertype1
        filtervalue1 = scoringcat.filtervalue1
        filtertype2 = scoringcat.filtertype2
        filtervalue2 = scoringcat.filtervalue2
        showexternal = scoringcat.showexternal
        minage = scoringcat.minage
        maxage = scoringcat.maxage
        # Ergebnisse ermitteln, die bei diesem Event zu dieser Wertung und Disziplin gehören
        if not scoringtype == 'FIN':
            result_query = Result.objects.filter(start__event__id=event.pk, start__competition__discipline__id=discipline.pk, start__competition__round=1)
        else:
            result_query = Result.objects.filter(start__event__id=event.pk, start__competition__discipline__id=discipline.pk)
        for result in result_query:
            # Bei mixed Disziplinen (m&w) muss das Alter des Teams für die Zuordnung berücksichtigt werden
            if (discipline.mixedgender == True):
                if (result.start.entry.team.ageforcomp < minage):
                    result_query = result_query.exclude(id=result.pk)
                elif (result.start.entry.team.ageforcomp > maxage):
                    result_query = result_query.exclude(id=result.pk)
            # Bei allen anderen Disziplinen muss das Alter des Teilnehmers für die Zuordnung berücksichtigt werden
            else:
                # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
                if ((maxage <= 10) and (result.start.person.age_at(event.startdate) <= maxage) and (result.start.person.otheragegroup == True)):
                    result_query = result_query.exclude(id=result.pk)
                    print('U11: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
                elif (result.start.person.age_at(event.startdate) < minage):
                    if not ((minage == 11) and (result.start.person.otheragegroup == True)):
                        result_query = result_query.exclude(id=result.pk)
                    else:
                        print('11+: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
                elif (result.start.person.age_at(event.startdate) > maxage):
                    result_query = result_query.exclude(id=result.pk)
        # Bei mixed Disziplinen (m&w) wird auf Basis des Teams gefiltert
        if (discipline.mixedgender == True):
            ### TODO ###
            # Wertungs-Filter für mixed Disziplinen implementieren
            rank_query = result_query
            withoutrank_query = Result.objects.none()
            ### ENDTODO ###
        # Bei allen anderen Disziplinen wird auf Basis der Personen gefiltert
        else:
            if not filtertype1 == None:
                kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
                rank_query = result_query.filter(**kwargs)
                withoutrank_query = result_query.exclude(**kwargs)
            elif not filtertype2 == None:
                kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
                rank_query = result_query.filter(**kwargs)
                withoutrank_query = result_query.exclude(**kwargs)
            else:
                rank_query = result_query
                withoutrank_query = Result.objects.none()

        # Wertungen auf Disziplinenbasis (pro Disziplin, sofern es keine Mixed Disziplin ist nur zwischen 'männlich' und 'weiblich' unterschieden, alle Wettbewerbe der Runde 1/AK)
        if scoringtype == 'DIS':
            if not discipline.mixedgender:
                # Bei Radlauf müssen U11 Fahrer aus der Juniorwertung ausgeschlossen werden
                if discipline.pk == 7:
                    # Ausgabe der 10m Radlauf Juniorwertung
                    printcomp = discipline.name + ' (10 m) ' + scoringcat.fullname + ' weiblich'
                    rank_query10 = rank_query
                    withoutrank_query10 = withoutrank_query
                    for result in rank_query10:
                        if result.start.person.age_at(dateforage) >= 11:
                            rank_query10 = rank_query10.exclude(id=result.pk)
                    for result in withoutrank_query10:
                        if result.start.person.age_at(dateforage) >= 11:
                            withoutrank_query10 = withoutrank_query10.exclude(id=result.pk)
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query10.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query10.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query10.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' (10 m) ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query10.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query10.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
                    # Ausgabe der 30m Radlauf Juniorwertung
                    rank_query30 = rank_query
                    withoutrank_query30 = withoutrank_query
                    for result in rank_query30:
                        if result.start.person.age_at(dateforage) < 11:
                            rank_query30 = rank_query30.exclude(id=result.pk)
                    for result in withoutrank_query30:
                        if result.start.person.age_at(dateforage) < 11:
                            withoutrank_query30 = withoutrank_query30.exclude(id=result.pk)
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query30.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query30.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query30.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query30.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query30.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
                else:
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
                    sorted_results = rank_results(rank_query.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_ext_results = withoutrank_query.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True))
                        for ext_result in sorted_ext_results:
                            ext_result.printcomp = printcomp
                        sorted_results += list(sorted_ext_results)
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_ext_results = withoutrank_query.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True))
                        for ext_result in sorted_ext_results:
                            ext_result.printcomp = printcomp
                        sorted_results += list(sorted_ext_results)
            else:
                printcomp = discipline.name + ' ' + scoringcat.fullname
                sorted_results = rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                if showexternal:
                    sorted_ext_results = withoutrank_query.order_by(F('value').asc(nulls_last=True))
                    for ext_result in sorted_ext_results:
                        ext_result.printcomp = printcomp
                    sorted_results += list(sorted_ext_results)
        
        # Wertungen auf Wettbewerbsbasis (pro Wettbewerb, alle Wettbewerbe der Runde 1/AK)
        elif scoringtype == 'COM':
            competitions = Competition.objects.filter(event__id=event.pk, discipline=discipline, ismerged=False).order_by('gender', 'maxage')
            sorted_results = list(Result.objects.none())
            for competition in competitions:
                printcomp = discipline.name + ' ' + competition.fullname
                sorted_results += rank_results(rank_query.filter(start__entry__competition=competition), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                if showexternal:
                    sorted_ext_results = withoutrank_query.filter(start__entry__competition=competition).order_by(F('value').asc(nulls_last=True))
                    for ext_result in sorted_ext_results:
                        ext_result.printcomp = printcomp
                    sorted_results += list(sorted_ext_results)
        
        # Gesamtwertung (disziplinenübergreifend, alle Wettbewerbe der Runde 1/AK) - noch nicht implementiert
        elif scoringtype == 'EVT':
            pass
            
        # Finalwertung (pro Disziplin, alle Wettbewerbe der Runde 2) - noch nicht implementiert
        elif scoringtype == 'FIN':
            pass
        
        # Ungültiger Bezeichner 
        else:
            sorted_results = Result.objects.none()
            error += 'Die gewählte Wertung kann aktuell noch nicht berechnet werden!'
        if request.method == "POST":
            # Wenn ein anderer Wettbewerb ausgewählt wird, Wettbewerb wechseln
            if 'edit_results' in request.POST:
                resultscoringcat = request.POST.get('scoringcat')
                scoringcat = get_object_or_404(Scoringcategory, pk=resultscoringcat)
                resultdiscipline = request.POST.get('discipline')
                discipline = get_object_or_404(Discipline, pk=resultdiscipline)
                return redirect('vunireg:execution_show_result', event_id=event_id, scoringcat_id=scoringcat.pk, discipline_id=discipline.pk)
        sorted_results_list = list(sorted_results)
        # Ausgabe
        content = {
            'scoringcat_list':scoringcat_list, 'competition_list':competition_query, 'discipline_list':discipline_query, 'result_list':sorted_results_list, 'scoringcat_id':scoringcat_id, 'discipline_id':discipline_id, 'event':event, 'response':response, 'error':error
        }
    else:
        # Ausgabe
        content = {
            'scoringcat_list':scoringcat_list, 'competition_list':competition_query, 'discipline_list':discipline_query, 'scoringcat_id':scoringcat_id, 'discipline_id':discipline_id, 'event':event, 'response':response, 'error':error
        }
    return render(request, "vunireg/results_view.html", content)

# Funktion zum manuellen Ausgeben von Ergebnislisten als PDF-Dateien
def generate_pdf(request):
    """Generate pdf."""
    event = get_object_or_404(Event, pk=2)
    competition_query = Competition.objects.filter(event__id=event.pk)
    discipline_query = Discipline.objects.filter(competition__in=competition_query).distinct()
    sorted_results = None
    # Model data
    scoringcat = get_object_or_404(Scoringcategory, pk=15)
#    discipline = get_object_or_404(Discipline, pk=2)
    # Allgemeine Werte
    dateforage = event.enddate
    # Filterwerte für die Wertung
    scoringtype = scoringcat.type
    filtertype1 = scoringcat.filtertype1
    filtervalue1 = scoringcat.filtervalue1
    filtertype2 = scoringcat.filtertype2
    filtervalue2 = scoringcat.filtervalue2
    showexternal = scoringcat.showexternal
    minage = scoringcat.minage
    maxage = scoringcat.maxage
    # Ausgabe für alle Disziplinen
    for discipline in discipline_query:
        # Ergebnisse ermitteln, die zu dieser Wertung und Disziplin gehören
        result_query = Result.objects.filter(start__competition__discipline__id=discipline.pk)
        for result in result_query:
            # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
            if ((maxage <= 10) and (result.start.person.age_at(dateforage) <= maxage) and (result.start.person.otheragegroup == True)):
                result_query = result_query.exclude(id=result.pk)
                print('U11: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
            elif (result.start.person.age_at(dateforage) < minage):
                if not ((minage == 11) and (result.start.person.otheragegroup == True)):
                    result_query = result_query.exclude(id=result.pk)
                else:
                    print('11+: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
            elif (result.start.person.age_at(dateforage) > maxage):
                result_query = result_query.exclude(id=result.pk)
                
        if not filtertype1 == None:
            kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
            rank_query = result_query.filter(**kwargs)
            withoutrank_query = result_query.exclude(**kwargs)
        elif not filtertype2 == None:
            kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
            rank_query = result_query.filter(**kwargs)
            withoutrank_query = result_query.exclude(**kwargs)
        else:
            rank_query = result_query
            withoutrank_query = Result.objects.none()
    
        # Bei Wertungen auf Disziplinenbasis wird - sofern es keine Mixed Disziplin ist - nur zwischen 'männlich' und 'weiblich' unterschieden
        if scoringtype == 'DIS':
            if not discipline.mixedgender:
                # Bei Radlauf müssen U11 Fahrer aus der Juniorwertung ausgeschlossen werden
                if discipline.pk == 7:
                    # Ausgabe der 10m Radlauf Juniorwertung
                    printcomp = discipline.name + ' (10 m) ' + scoringcat.fullname + ' weiblich'
                    rank_query10 = rank_query
                    withoutrank_query10 = withoutrank_query
                    for result in rank_query10:
                        if result.start.person.age_at(dateforage) >= 11:
                            rank_query10 = rank_query10.exclude(id=result.pk)
                    for result in withoutrank_query10:
                        if result.start.person.age_at(dateforage) >= 11:
                            withoutrank_query10 = withoutrank_query10.exclude(id=result.pk)
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query10.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query10.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query10.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' (10 m) ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query10.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query10.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
                    # Ausgabe der 30m Radlauf Juniorwertung
                    rank_query30 = rank_query
                    withoutrank_query30 = withoutrank_query
                    for result in rank_query30:
                        if result.start.person.age_at(dateforage) < 11:
                            rank_query30 = rank_query30.exclude(id=result.pk)
                    for result in withoutrank_query30:
                        if result.start.person.age_at(dateforage) < 11:
                            withoutrank_query30 = withoutrank_query30.exclude(id=result.pk)
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query30.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query30.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query30.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query30.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query30.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
                else:
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
            else:
                printcomp = discipline.name + ' ' + scoringcat.fullname
                if sorted_results == None:
                    sorted_results = rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                else:
                    sorted_results += rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                if showexternal:
                    sorted_results += list(withoutrank_query.order_by(F('value').asc(nulls_last=True)))
    
        # Bei Wertungen auf Wettbewerbsbasis wird zwischen allen Wettbewerben unterschieden
        elif scoringtype == 'COM':
            competitions = Competition.objects.filter(event__id=event.pk, discipline=discipline, ismerged=False).order_by('gender', 'maxage')
            if sorted_results == None:
                sorted_results = list(Result.objects.none())
            else:
                sorted_results += list(Result.objects.none())
            for competition in competitions:
                sorted_results += rank_results(rank_query.filter(start__entry__competition=competition), discipline.ranking, discipline.isgroupdisc, discipline.name + ' ' + competition.fullname, scoringcat.usequali)
                if showexternal:
                    sorted_results += list(withoutrank_query.filter(start__entry__competition=competition).order_by(F('value').asc(nulls_last=True)))
        else:
            sorted_results = Result.objects.none()
            error += 'Die gewählte Wertung kann aktuell noch nicht berechnet werden!'

    # Rendered
    html_string = render_to_string('vunireg/pdf.html', {'result_list':sorted_results})
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    result = html.write_pdf()

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())

    return response

# Funktion zum Ausgeben von Ergebnislisten als PDF-Dateien
def print_resultlist(request, event_id, scoringcat_id, discipline_id):
    """Generate pdf."""
    event = get_object_or_404(Event, pk=event_id)
    competition_query = Competition.objects.filter(event__id=event.pk)
    discipline_query = Discipline.objects.filter(competition__in=competition_query).distinct()
    sorted_results = None
    # Model data
    scoringcat = get_object_or_404(Scoringcategory, pk=scoringcat_id)
    discipline = get_object_or_404(Discipline, pk=discipline_id)
    # Allgemeine Werte
    dateforage = event.enddate
    # Filterwerte für die Wertung
    scoringtype = scoringcat.type
    filtertype1 = scoringcat.filtertype1
    filtervalue1 = scoringcat.filtervalue1
    filtertype2 = scoringcat.filtertype2
    filtervalue2 = scoringcat.filtervalue2
    showexternal = scoringcat.showexternal
    minage = scoringcat.minage
    maxage = scoringcat.maxage
    # Ausgabe für alle Disziplinen
    for discipline in discipline_query:
        # Ergebnisse ermitteln, die zu dieser Wertung und Disziplin gehören
#        result_query = Result.objects.filter(start__competition__discipline__id=discipline.pk)
        # Ergebnisse ermitteln, die bei diesem Event zu dieser Wertung und Disziplin gehören
        if not scoringtype == 'FIN':
            result_query = Result.objects.filter(start__event__id=event.pk, start__competition__discipline__id=discipline.pk, start__competition__round=1)
        else:
            result_query = Result.objects.filter(start__event__id=event.pk, start__competition__discipline__id=discipline.pk)
        for result in result_query:
            # Bei U11 Fahrern muss unter Umständen ein abweichender Start in der U13 berücksichtigt werden
            if ((maxage <= 10) and (result.start.person.age_at(dateforage) <= maxage) and (result.start.person.otheragegroup == True)):
                result_query = result_query.exclude(id=result.pk)
                print('U11: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
            elif (result.start.person.age_at(dateforage) < minage):
                if not ((minage == 11) and (result.start.person.otheragegroup == True)):
                    result_query = result_query.exclude(id=result.pk)
                else:
                    print('11+: U11 Fahrer, der in der U13 startet: ' + result.start.person.forename)
            elif (result.start.person.age_at(dateforage) > maxage):
                result_query = result_query.exclude(id=result.pk)
                
        if not filtertype1 == None:
            kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
            rank_query = result_query.filter(**kwargs)
            withoutrank_query = result_query.exclude(**kwargs)
        elif not filtertype2 == None:
            kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
            rank_query = result_query.filter(**kwargs)
            withoutrank_query = result_query.exclude(**kwargs)
        else:
            rank_query = result_query
            withoutrank_query = Result.objects.none()
    
        # Bei Wertungen auf Disziplinenbasis wird - sofern es keine Mixed Disziplin ist - nur zwischen 'männlich' und 'weiblich' unterschieden
        if scoringtype == 'DIS':
            if not discipline.mixedgender:
                # Bei Radlauf müssen U11 Fahrer aus der Juniorwertung ausgeschlossen werden
                if discipline.pk == 7:
                    # Ausgabe der 10m Radlauf Juniorwertung
                    printcomp = discipline.name + ' (10 m) ' + scoringcat.fullname + ' weiblich'
                    rank_query10 = rank_query
                    withoutrank_query10 = withoutrank_query
                    for result in rank_query10:
                        if result.start.person.age_at(dateforage) >= 11:
                            rank_query10 = rank_query10.exclude(id=result.pk)
                    for result in withoutrank_query10:
                        if result.start.person.age_at(dateforage) >= 11:
                            withoutrank_query10 = withoutrank_query10.exclude(id=result.pk)
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query10.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query10.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query10.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' (10 m) ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query10.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query10.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
                    # Ausgabe der 30m Radlauf Juniorwertung
                    rank_query30 = rank_query
                    withoutrank_query30 = withoutrank_query
                    for result in rank_query30:
                        if result.start.person.age_at(dateforage) < 11:
                            rank_query30 = rank_query30.exclude(id=result.pk)
                    for result in withoutrank_query30:
                        if result.start.person.age_at(dateforage) < 11:
                            withoutrank_query30 = withoutrank_query30.exclude(id=result.pk)
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query30.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query30.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query30.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query30.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query30.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
                else:
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' weiblich'
                    if sorted_results == None:
                        sorted_results = rank_results(rank_query.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    else:
                        sorted_results += rank_results(rank_query.filter(start__person__gender='f'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query.filter(start__person__gender='f').order_by(F('value').asc(nulls_last=True)))
                    printcomp = discipline.name + ' ' + scoringcat.fullname + ' männlich'
                    sorted_results += rank_results(rank_query.filter(start__person__gender='m'), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                    if showexternal:
                        sorted_results += list(withoutrank_query.filter(start__person__gender='m').order_by(F('value').asc(nulls_last=True)))
            else:
                printcomp = discipline.name + ' ' + scoringcat.fullname
                if sorted_results == None:
                    sorted_results = rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                else:
                    sorted_results += rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                if showexternal:
                    sorted_results += list(withoutrank_query.order_by(F('value').asc(nulls_last=True)))
    
        # Bei Wertungen auf Wettbewerbsbasis wird zwischen allen Wettbewerben unterschieden
        elif scoringtype == 'COM':
            competitions = Competition.objects.filter(event__id=event.pk, discipline=discipline, ismerged=False).order_by('gender', 'maxage')
            if sorted_results == None:
                sorted_results = list(Result.objects.none())
            else:
                sorted_results += list(Result.objects.none())
            for competition in competitions:
                printcomp = discipline.name + ' ' + competition.fullname
                sorted_results += rank_results(rank_query.filter(start__entry__competition=competition), discipline.ranking, discipline.isgroupdisc, printcomp, scoringcat.usequali)
                if showexternal:
                    sorted_ext_results = withoutrank_query.filter(start__entry__competition=competition).order_by(F('value').asc(nulls_last=True))
                    for ext_result in sorted_ext_results:
                        ext_result.printcomp = printcomp
                    sorted_results += list(sorted_ext_results)
        else:
            sorted_results = Result.objects.none()
            error += 'Die gewählte Wertung kann aktuell noch nicht berechnet werden!'

    # Rendered
    html_string = render_to_string('vunireg/pdf.html', {'result_list':sorted_results})
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    result = html.write_pdf()

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())

    return response

    
# Funktion zum manuellen Ausgeben von Teilnehmer-Urkunden als PDF-Dateien
def generate_certificate_pdf(request):
    """Generate pdf."""
    # Allgemeine Werte
    event = get_object_or_404(Event, pk=2)
    club = get_object_or_404(Club, pk=10)
    person_list = Person.objects.filter(club=club.pk)
    print_results = None
    # Stichtag für die Altersberechnung (i.d.R. der erste Tag der Veranstaltung => event.startdate)
    dateforage = event.enddate
    # Wertung auf deren Basis die Teilnehmerurkunde ausgestellt werden soll (i.d.R. die AK-Wertung => pk=14)
    scoringcat = get_object_or_404(Scoringcategory, pk=14)
    
    # Urkunde für alle Teilnehmer des Vereins erstellen
    for person in person_list:
        # Ermitteln aller Ergebnisse, die zum Teilnehmer gehören
        personal_results = Result.objects.filter(person__id=person.pk)
        # Filterwerte für die Wertung
        scoringtype = scoringcat.type
        filtertype1 = scoringcat.filtertype1
        filtervalue1 = scoringcat.filtervalue1
        filtertype2 = scoringcat.filtertype2
        filtervalue2 = scoringcat.filtervalue2
        showexternal = scoringcat.showexternal
        minage = scoringcat.minage
        maxage = scoringcat.maxage
        #Sortieren der Ergebnisse nach Disziplinen
        personal_results = personal_results.order_by(F('discipline').asc(nulls_last=True))
    
        # Für jedes Ergebnis die Platzierung bestimmen und für die Ausgabe speichern
        for result in personal_results:
            competition = result.start.entry.competition
            discipline = result.start.entry.competition.discipline
            competition_results = Result.objects.filter(start__competition__id=competition.pk)
            # Je nach Filterwerten der Wertungskategorie die Ergebnisse filtern
            if not filtertype1 == None:
                kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
                rank_query = competition_results.filter(**kwargs)
            elif not filtertype2 == None:
                kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
                rank_query = competition_results.filter(**kwargs)
            else:
                rank_query = competition_results
            # Berechnen der Platzierungen
            sorted_results = rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, competition, scoringcat.usequali)
            # Für die Ausgabe hinzufügen der Platzierung
            temp_result = next((x for x in sorted_results if x.person == person), None)
            result.rank = temp_result.rank
            
        if print_results == None:
            print_results = list(personal_results)
        else:
            print_results += list(personal_results)
    
    # Rendered
    font_config = FontConfiguration()
    css = CSS(string='''
    @font-face { font-family: 'ITC Flora Std';
             src: local ('ITC Flora Std')
                  url(file:///Users/Jan/Documents/Sites/ServerDocuments/vuni/static/font/FloraStd-Medium.otf); }
    }
    .certificate_participantblock { font-family: 'ITC Flora Std' }''', font_config=font_config)
    html_string = render_to_string('vunireg/pdf_certificate_participant.html', {'result_list':print_results})
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    result = html.write_pdf(stylesheets=[css], font_config=font_config)

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=participant_certificate.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
        
    return response
    
# Funktion zum Ausgeben von Teilnehmer-Urkunden als PDF-Dateien
def print_compcert_pdf(request, event_id, club_id):
    """Generate pdf."""
    # Allgemeine Werte
    event = get_object_or_404(Event, pk=event_id)
    club = get_object_or_404(Club, pk=club_id)
    ### TODO: Wertungskategorie für die Urkunden dynamisch übergeben ###
    # Wertung auf deren Basis die Teilnehmerurkunde ausgestellt werden soll (i.d.R. die AK-Wertung)
    scoringcat = get_object_or_404(Scoringcategory, pk=24)
    person_list = Person.objects.filter(club=club.pk)
    print_results = None
    ### TODO: Stichtag dynamisch übergeben; Auswahl beim Erstellen des Wettkampfes ###
    # Stichtag für die Altersberechnung (i.d.R. der erste Tag der Veranstaltung => event.startdate)
    dateforage = event.enddate
    
    # Urkunde für alle Teilnehmer des Vereins erstellen
    for person in person_list:
        # Ermitteln aller Ergebnisse, die zum Teilnehmer gehören
        personal_results = Result.objects.filter(start__event__id=event.pk, person__id=person.pk)
        # Filterwerte für die Wertung
        scoringtype = scoringcat.type
        filtertype1 = scoringcat.filtertype1
        filtervalue1 = scoringcat.filtervalue1
        filtertype2 = scoringcat.filtertype2
        filtervalue2 = scoringcat.filtervalue2
        showexternal = scoringcat.showexternal
        minage = scoringcat.minage
        maxage = scoringcat.maxage
        #Sortieren der Ergebnisse nach Disziplinen
        personal_results = personal_results.order_by(F('discipline').asc(nulls_last=True))
        # Für jedes Ergebnis die Platzierung bestimmen und für die Ausgabe speichern
        for result in personal_results:
            competition = result.start.entry.competition
            discipline = result.start.entry.competition.discipline
            competition_results = Result.objects.filter(start__event__id=event.pk, start__entry__competition__id=competition.pk)
            # Je nach Filterwerten der Wertungskategorie die Ergebnisse filtern
            if not filtertype1 == None:
                kwargs = {'start__person__{0}'.format(filtertype1): filtervalue1,}
                rank_query = competition_results.filter(**kwargs)
            elif not filtertype2 == None:
                kwargs = {'start__person__{0}'.format(filtertype2): filtervalue2,}
                rank_query = competition_results.filter(**kwargs)
            else:
                rank_query = competition_results
            # Berechnen der Platzierungen
            sorted_results = rank_results(rank_query, discipline.ranking, discipline.isgroupdisc, competition, scoringcat.usequali)
            # Für die Ausgabe hinzufügen der Platzierung
            temp_result = next((x for x in sorted_results if x.person == person), None)
            if temp_result:
                result.rank = temp_result.rank
            else:
                result.rank = ''
            
        if print_results == None:
            print_results = list(personal_results)
        else:
            print_results += list(personal_results)
        
    # Rendered
    font_config = FontConfiguration()
    css = CSS(string='''
    @font-face { font-family: 'ITC Flora Std';
             src: local ('ITC Flora Std')
                  url(file:///Users/Jan/Documents/Sites/ServerDocuments/vuni/static/font/FloraStd-Medium.otf); }
    }
    .certificate_participantblock { font-family: 'ITC Flora Std' }''', font_config=font_config)
    html_string = render_to_string('vunireg/pdf_certificate_participant.html', {'result_list':print_results})
    html = HTML(string=html_string, base_url=request.build_absolute_uri())
    result = html.write_pdf(stylesheets=[css], font_config=font_config)

    # Creating http response
    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=participant_certificate.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
        
    return response