from django.urls import path, include, re_path
from django.conf.urls import url, include

from . import views

app_name = 'vunireg'
urlpatterns = [
    path('', views.index, name='index'),
    url('home', views.home, name="home"),
    path('test', views.test, name='test'),
    path('add/', views.event_add, name='event_add'),
    path('<event_id>/event/', views.event, name='event'),
    path('<event_id>/event/eventdata/', views.event_edit_eventdata, name='event_edit_eventdata'),
    path('<event_id>/event/competition/', views.event_edit_competition, name='event_edit_competition'),
    path('<event_id>/event/scoringcategory/<scoringcat_id>', views.event_edit_scoringcat, name='event_edit_scoringcat'),
    path('<event_id>/event/qualification/<qualification_id>', views.event_edit_qualification, name='event_edit_qualification'),
    path('<event_id>/entry/', views.entrys, name='entrys'),
    path('<event_id>/entrys/teilnehmer/', views.person_list, name='person_list'),
    path('<event_id>/entrys/teilnehmer/import/', views.person_csv, name='person_csv'),
    path('<event_id>/entrys/teilnehmer/add/', views.person_add, name='person_add'),
    path('<event_id>/entrys/teilnehmer/editm/<person_id>/', views.person_editm, name='person_editm'),
    path('<event_id>/entrys/teams/', views.entrys_team_list, name='entrys_team_list'),
    path('<event_id>/entrys/team/edit/<team_id>', views.entrys_team_edit, name='entrys_team_edit'),
    path('<event_id>/entrys/clubs/', views.club_list, name='club_list'),
    path('<event_id>/entrys/club/edit/<club_id>', views.entrys_club_edit, name='entrys_club_edit'),
    path('<event_id>/entrys/club/merge/<club1_id>/<club2_id>', views.entrys_club_merge, name='entrys_club_merge'),
    path('<event_id>/entrys/meldungen/', views.entrys_entry_list, name='entrys_entry_list'),
    path('<event_id>/starts/', views.start, name='start'),
    path('<event_id>/starts/competition/', views.competition_list, name='competition_list'),
    path('<event_id>/starts/competition/?del=<competition_id>', views.del_singlecompetition, name='del_singlecompetition'),
    path('<event_id>/starts/competition/?merge=<competition_id>', views.merge_singlecompetition, name='merge_singlecompetition'),
    path('<event_id>/starts/competition/calc_entry', views.competition_list, name='competition_list'),
    path('<event_id>/starts/startnr/', views.start_startnr, name='start_startnr'),
    path('<event_id>/starts/startnr?del=<person_id>', views.startnr_clr, name='startnr_clr'),
    path('<event_id>/starts/seeding/', views.start_seed_overview, name='start_seed_overview'),
    path('<event_id>/starts/seeding/<discipline_id>', views.start_seed_listview, name='start_seed_listview'),
    path('<event_id>/execution/', views.execution, name='execution'),
    path('<event_id>/execution/results/show/<scoringcat_id>/<discipline_id>', views.execution_show_result, name='execution_show_result'),
    path('<event_id>/execution/results/edit/<competition_id>', views.execution_edit_result, name='execution_edit_result'),
    path('<event_id>/execution/results/import/', views.import_result_csv, name='import_result_csv'),
    path('<event_id>/execution/results/print_resultlist/<scoringcat_id>/<discipline_id>', views.print_resultlist, name='print_resultlist'),
    path('<event_id>/execution/results/print_compcert/<club_id>', views.print_compcert_pdf, name='print_compcert_pdf'),
    path('pdf/', views.generate_pdf, name='generate_pdf'),
    path('urkunde/', views.generate_certificate_pdf, name='generate_certificate_pdf'),
    path('<event_id>/infield/iuf/<start_id>', views.infield_iuf_slalom, name='infield_iuf_slalom'),
    path('infield_serialsend', views.infield_serialsend, name='infield_serialsend'),
    path('infield_serialread', views.infield_serialread, name='infield_serialread'),
]

#(?P<pk>\d+)/$