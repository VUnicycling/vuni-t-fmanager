from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
import datetime
import csv
import types
from django.db.models import F, Func

from django.template.loader import render_to_string
from django.conf import settings
#from weasyprint import HTML, CSS
import tempfile

from vunireg.models import Event, Person, Team, Club, Entry, Discipline, Agegroup, Competition, Start, Scoringcategory, Qualification, Result
from .filters import PersonFilter, ClubFilter, EntryFilter, CompetitionFilter
from .forms import PersonEditForm, EntryEditInlineForm, TeamEditForm, ClubEditForm



# Funktion, zur Ausgabe der Übersichtsseite für den Bereich Meldungen
def entrys(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    entry_query = Entry.objects.filter(event__id=event.pk)
    person_query = Person.objects.filter(entry__in=entry_query).distinct()
    club_query = Club.objects.filter(person__in=person_query).distinct()
    participans = types.SimpleNamespace()
    participans.total = person_query.count()
    participans.male = person_query.filter(gender='m').count()
    participans.female = person_query.filter(gender='f').count()
#    participans.U15total = person_query.count()
#    participans.U15male = person_query.filter(gender='m').count()
#    participans.U15female = person_query.filter(gender='f').count()
#    participans.15UPtotal = person_query.count()
#    participans.15UPmale = person_query.filter(gender='m').count()
#    participans.15UPfemale = person_query.filter(gender='f').count()
    entrys = types.SimpleNamespace()
    entrys.single = entry_query.filter(discipline__isgroupdisc=1).count()
    entrys.relay = entry_query.filter(team__isnull=False).count()
    entrys.total = entrys.single + entrys.relay
    clubs = types.SimpleNamespace()
    clubs.total = club_query.count()
    # Ausgabe
    content = {
        "participans":participans, "entrys":entrys, 'clubs':clubs, 'error':error, 'response':response, 'event':event,
    }
    return render(request, "vunireg/entrys_overview.html", content)

# Funktion, zur Ausgabe der Seite zum Verwalten der Teilnehmer
def person_list(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    entry_query = Entry.objects.filter(event__id=event.pk)
    person_query = Person.objects.filter(entry__in=entry_query).distinct()
    person_filter = PersonFilter(request.GET, queryset = person_query)
    # Ausgabe
    content = {
        'filter':person_filter, 'error':error, 'response':response, 'event':event,
    }
    return render(request, 'vunireg/entrys_person_list.html', content)
    
# Funktion, zur Ausgabe der Seite zum Verwalten der Teams
def entrys_team_list(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    entry_query = Entry.objects.filter(event__id=event.pk)
    team_query = Team.objects.filter(event__id=event.pk)
    # Teams anhand der Meldungen automatisch erstellen
    if(request.GET.get('update_teams')):
        entrys_update_teams(event.pk)
        response += 'Alle Teams wurden auf Basis der Meldungen aktualisiert!' + '</br>'
        # Warnmeldung ausgeben, falls es noch Staffel-Meldungen gibt, die keiner Staffel zugeordnet sind
        entrys_for_teams_query = entry_query.filter(discipline__isgroupdisc__gt=1, person__isnull=False)
        competition_error = len(entrys_for_teams_query.filter(entry_team=None))
        if competition_error == 0:
            response += 'Alle Einzel-Meldungen für eine Staffel sind einem Staffel-Team zugeordnet.' + '</br>'
    # Für jede Staffel die aktuelle Teilnehmerzahl ermitteln
    for team in team_query:
        team.members = team.team_entries.count()
    # Warnmeldung ausgeben, falls es noch Staffel-Meldungen gibt, die keiner Staffel zugeordnet sind
    entrys_for_teams_query = entry_query.filter(discipline__isgroupdisc__gt=1, person__isnull=False)
    competition_error = len(entrys_for_teams_query.filter(entry_team=None))
    if competition_error > 0:
        error += 'ACHTUNG: Es gibt noch ' + str(competition_error) + ' Einzel-Meldung(en) für eine Staffel, die keinem Staffel-Team zugeordnet ist/sind!' + '<br />'
    # Ausgabe
    content = {
        'team_list':team_query, 'error':error, 'response':response, 'event':event,
    }
    return render(request, 'vunireg/entrys_team_list.html', content)

# Funktion, zur Ausgabe der Seite zum Verwalten der Vereine
def club_list(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    entry_query = Entry.objects.filter(event__id=event.pk)
    person_query = Person.objects.filter(entry__in=entry_query).distinct()
    club_query = Club.objects.filter(person__in=person_query).distinct()
    # Anzahl der Teilnehmer & Anzahl der Meldungen pro Verein ermitteln
    for club in club_query:
        club.participants = len(person_query.filter(club=club))
        club.entrys = len(entry_query.filter(person__club=club))
    # Ausgabe
    content = {
        "club_list": club_query, 'error':error, 'response':response, 'event':event,
    }
    return render(request, "vunireg/entrys_club_list.html", content)

# Funktion, zur Ausgabe einer Liste aller Meldungen
def entrys_entry_list(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    #    entry_list = Entry.objects.all()
    entry_list = Entry.objects.filter(event__id=event.pk).order_by('person__club', 'person__surname', 'person__forename')
    entry_filter = EntryFilter(request.GET, queryset = entry_list)
    # Ausgabe
    content = {
        'filter':entry_filter, "entry_list":entry_list, 'error':error, 'response':response, 'event':event,
    }
    return render(request, "vunireg/entrys_entry_list.html", content)

# Funktion, die das Hinzufügen einzelner oder mehrerer Meldungen und Teilnehmer per CSV Datei ermöglicht
def person_csv(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    csvfile = request.POST.get('csvfile', False)
    response = ''
    error = ''
    text = ''
    if csvfile:
        n = 0
        for row in csvfile.splitlines():
            data = row.split(',')
            txt = ''
            n += 1
            datacount = len(data)
            # Es werden mindestens 6 Elemente in der Eingabe benötigt, um einen gültigen Teilnehmer anzulegen
            if datacount < 6:
                error += 'ERROR: Die Eingabe in Reihe ' + str(n) + ' enthält zu wenig Daten'
                text += 'ERROR: Die Eingabe enthält zu wenig Daten\r\n'
                text += ','.join(data) + '\r\n'
                break

            # überprüfen, ob Verein vorhanden
            try:
                v = Club.objects.get(clubname__iexact=data[5])
                txt += 'Verein vorhanden - '
            except Club.DoesNotExist:
                txt += 'Verein nicht vorhanden - '
                if data[5] == '':
                    v = Club(clubname=('vereinslos_' + data[0]), individualstart=True)
                    v.save()
                else:
                    v = Club(clubname=data[5], individualstart=False)
                    v.save()

            # überprüfen, ob Teilnehmer vorhanden
            try:
                t = Person.objects.get(surname__iexact=data[0], forename__iexact=data[1], dateofbirth=data[3])
                txt += 'Teilnehmer vorhanden - '
            except Person.DoesNotExist:
                txt += 'Teilnehmer nicht vorhanden - '
                t = Person(surname=data[0], forename=data[1], gender=data[2], dateofbirth=data[3], custom1=data[4], club=v)
                t.save()

            # Bei mehr als 6 Elementen werden zusätzlich zu den Teilnehmerdaten noch Meldungen übergeben
            if datacount > 6:
                i = 0
                while i < (datacount-6):
                    i += 1
                    # überprüfen, ob Disziplin vorhanden
                    try:
                        d = Discipline.objects.get(name__iexact=data[(5+i)])
                        txt += 'Disziplin vorhanden - '
                    except Discipline.DoesNotExist:
                        try:
                            d = Discipline.objects.get(code__iexact=data[(5+i)])
                            txt += 'Disziplin vorhanden - '
                        except Discipline.DoesNotExist:
                            error += 'ERROR: Die Disziplin ' + data[(5+i)] + ' ist nicht bekannt!'
                            text += 'ERROR: Die Disziplin ' + data[(5+i)] + ' ist nicht bekannt!\r\n'
                            break
                   #     d = Discipline(name=data[(5+i)], group=0, ranking=0)
                   #     d.save()
                    i += 1
                    # überprüfen, ob Meldung vorhanden
                    try:
                        m = Entry.objects.get(event=event, person=t, discipline=d)
                        if m.value==data[(5+i)]:
                            txt += 'Meldung breits vorhanden!'
                        else:
                            m.value=data[(5+i)]
                            m.save()
                            txt += 'Meldung breits vorhanden, Meldezeit aktualisiert!'
                    except Entry.DoesNotExist:
                        txt += 'Meldung noch nicht vorhanden & angelegt!'
                        m = Entry(event=event, person=t , discipline=d, value=data[(5+i)])
                        m.save()
                    
                    text += txt + '\r\n'
                    text += ','.join(data) + '\r\n'
                    txt = ''
            else:
                text += txt + '\r\n'
                text += ','.join(data) + '\r\n'
    # Ausgabe
    content = {
        'text':text, 'error':error, 'response':response, 'event':event,
    }
    return render(request, "vunireg/person_csv.html", content )

# Funktion, die das Hinzufügen einzelner Teilnehmer ermöglicht
def person_add(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    ### To Do ###
    ### Einfügen und Übergabe von Fehler- und Rückmeldungen an die HTML Seite
    ### To Do ###
    if request.method == 'POST':
        i = ''
        pform = PersonEditForm(request.POST)
        if pform.is_valid():
                person = pform.save()
                return redirect('vunireg:person_editm', event_id = event.pk, person_id = person.pk)
        else:
            print(pform.errors)
            # Ausgabe
            content = {
                'pform':pform, 'error':error, 'response':response, 'event':event,
            }
            return render(request, 'vunireg/entrys_person_add_form.html', content )
    else:
        pform = PersonEditForm()
    # Ausgabe
    content = {
        'pform':pform, 'error':error, 'response':response, 'event':event,
    }
    return render(request, 'vunireg/entrys_person_add_form.html', content )
   
# Funktion, die das Bearbeiten einzelner Teilnehmer und der zugehörigen Meldungen ermöglicht
def person_editm(request, event_id, person_id):
    event = get_object_or_404(Event, pk=event_id)
    person = get_object_or_404(Person, pk = person_id)
    pform = PersonEditForm(request.POST or None, instance = person)
    mform = EntryEditInlineForm(request.POST or None, instance = person, event_id = event.pk)
    response = ''
    error = ''
    if request.method == "POST":
        rewritequery = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
        for form in mform:
            form.fields['discipline'].queryset = rewritequery            
        if pform.is_valid() and mform.is_valid():
            ### To Do ###
            ### Übergabe des Response Textes an die HTML Seite
            ### To Do ###
            pform.save()
            #mform.save()
            new_instances = mform.save(commit=False)
            for obj in mform.deleted_objects:
                response += 'Die folgende Meldung wurde gelöscht: ' + obj.discipline.name
                print(response)
                obj.delete()
            for new_instance in new_instances:
                new_instance.event = event
                new_instance.save()
                response += 'Die folgende Meldung wurde hinzugefügt oder bearbeitet: ' + new_instance.discipline.name
                print(response)
            response += 'Teilnehmer gespeichert'
            return redirect('vunireg:person_editm', person_id = person.id, event_id = event.pk)
        else:
            error += 'Das Formular konnte nicht validiert werden!'
            print(response)
            print(pform.errors)
            print(mform.errors)
            # Ausgabe
            content = {
                'pform': pform, 'mform': mform, 'error':error, 'response':response, 'event':event,
            }
            return render(request, 'vunireg/entrys_person_edit_form.html', content)
    else:
        pform = PersonEditForm(instance = person)
        mform = EntryEditInlineForm(instance = person, event_id = event.pk)
        rewritequery = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk)).distinct()
        for form in mform:
            form.fields['discipline'].queryset = rewritequery
        # Ausgabe
        content = {
            'pform': pform, 'mform': mform, 'error':error, 'response':response, 'event':event,
        }
        return render(request, 'vunireg/entrys_person_edit_form.html', content)

# Funktion, die das Anlegen und Bearbeiten einzelner Teams ermöglicht
def entrys_team_edit(request, event_id, team_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    text = types.SimpleNamespace()
    if not team_id == 'None':
        text.heading = 'Team bearbeiten'
        team = get_object_or_404(Team, pk=team_id)
        entry = team.entry
        team_entries = team.team_entries.all()
        if request.method == "POST":
            if 'save_team' in request.POST:
                team_form = TeamEditForm(request.POST or None, instance=team)
                if team_form.has_changed():
                    if team_form.is_valid():
                        team = team_form.save(commit=False)
                        team.event = event
                        team.save()
                        response += 'Team gespeichert'
                        if team.discipline != entry.discipline:
                            entry.discipline = team.discipline
                            entry.save()
                            response += 'Disziplin geändert'
                            print('Disziplin geändert')
                    else:
                        error += 'Das Formular konnte nicht validiert werden!'
                        ### To Do ###
                        ### Übergabe aller Form-Errors an die HTML Seite ?
                        print(team_form.errors)
                        ### To Do ###
            elif 'delete_team' in request.POST:
                team.delete()
                ### TODO ###
                ### Rückmeldungen über das redirect hinaus weitergeben und #popup1 in der URL entfernen
                response += 'Team ' + team.name + ' wurde erfolgreich gelöscht.'
                return redirect('vunireg:entrys_team_list', event_id=event.pk)
                ### ENDTODO ###
            else:
                team_form = TeamEditForm(instance=team)
                error += 'Kein gültiges POST Argument gefunden!'
        else:
            team_form = TeamEditForm(instance=team)
    else:
        text.heading = 'Team anlegen'
        team = None
        team_entries = None
        team_form = TeamEditForm(request.POST or None)
        if request.method == "POST":
            if team_form.is_valid():
                team = team_form.save(commit=False)
                team.event = event
                team.save()
                entry = Entry(event=event, discipline=team.discipline, team=team)
                entry.save()
                response += 'Team gespeichert'
                team_form = TeamEditForm(instance=team)
                ### TODO ###
                ### Rückmeldungen über das redirect hinaus weitergeben
                return redirect('vunireg:entrys_team_edit', event_id=event.pk, team_id=team.pk)
                ### ENDTODO ###
            else:
                error += 'Das Formular konnte nicht validiert werden!'
                ### To Do ###
                ### Übergabe aller Form-Errors an die HTML Seite ?
                print(team_form.errors)
                ### To Do ###
        else:
            team_form = TeamEditForm()
    # Ausgabe
    content = {
        'team_form':team_form, 'team':team, 'team_entries':team_entries, 'text':text, 'error':error, 'response':response, 'event':event,
    }
    print(request)
    return render(request, 'vunireg/entrys_team_edit_form.html', content)

# Funktion, die das Anlegen und Bearbeiten einzelner Vereine ermöglicht
def entrys_club_edit(request, event_id, club_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    text = types.SimpleNamespace()
    if not club_id == 'None':
        text.heading = 'Verein bearbeiten'
        club = get_object_or_404(Club, pk=club_id)
        club_form = ClubEditForm(request.POST or None, instance=club)
        if request.method == "POST":
            if club_form.is_valid():
                club = club_form.save()
                ### To Do ###
                ### Übergabe des Response Textes an die HTML Seite
                response += 'Verein gespeichert'
                ### To Do ###
                return redirect('vunireg:entrys_club_edit', event_id=event.pk, club_id=club.pk)
            else:
                error += 'Das Formular konnte nicht validiert werden!'
                ### To Do ###
                ### Übergabe aller Form-Errors an die HTML Seite
                print(club_form.errors)
                ### To Do ###
        else:
            club_form = ClubEditForm(instance=club)
    else:
        text.heading = 'Verein anlegen'
        club_form = ClubEditForm(request.POST or None)
        if request.method == "POST":
            if club_form.is_valid():
                club = club_form.save()
                ### To Do ###
                ### Übergabe des Response Textes an die HTML Seite
                response += 'Verein gespeichert'
                ### To Do ###
                return redirect('vunireg:entrys_club_edit', event_id=event.pk, club_id=club.pk)
            else:
                error += 'Das Formular konnte nicht validiert werden!'
                ### To Do ###
                ### Übergabe aller Form-Errors an die HTML Seite
                print(club_form.errors)
                ### To Do ###
        else:
            club_form = ClubEditForm()
    # Ausgabe
    content = {
        'club_form':club_form, 'text':text, 'error':error, 'response':response, 'event':event,
    }
    return render(request, 'vunireg/entrys_club_edit_form.html', content)

# Funktion, die das Zusammenlegen von zwei Vereinen ermöglicht
def entrys_club_merge(request, event_id, club1_id, club2_id):
    event = get_object_or_404(Event, pk=event_id)
    response = ''
    error = ''
    text = types.SimpleNamespace()
    club_query = Club.objects.all()
    # Bestimmen der Teilnehmer für die Anzeige im Frontend
    if not club1_id == 'None':
        merge_to_club = get_object_or_404(Club, pk=club1_id)
        merge_to_club_persons = Person.objects.filter(club=merge_to_club)
    else:
        merge_to_club_persons = Person.objects.none()
    if not club2_id == 'None':
        club_to_merge = get_object_or_404(Club, pk=club2_id)
        club_to_merge_persons = Person.objects.filter(club=club_to_merge)
    else:
        club_to_merge_persons = Person.objects.none()
    # Auswerten der Benutzereingabe
    if request.method == "POST":
        if 'change_club1' in request.POST:
            club1_id = request.POST.get('club1')
            merge_to_club = get_object_or_404(Club, pk=club1_id)
            return redirect('vunireg:entrys_club_merge', event_id=event_id, club1_id=merge_to_club.pk, club2_id=club2_id)
        if 'change_club2' in request.POST:
            club2_id = request.POST.get('club2')
            club_to_merge = get_object_or_404(Club, pk=club2_id)
            return redirect('vunireg:entrys_club_merge', event_id=event_id, club1_id=club1_id, club2_id=club_to_merge.pk)
        if 'merge_clubs' in request.POST:
            if (club1_id == 'None' or club2_id == 'None'):
                # Fehler: Es müssen zwei Vereine zum zusammenlegen ausgewählt werden
                error += 'Fehler: Es müssen zwei Vereine zum Zusammenlegen ausgewählt werden!' + '<br />'
            else:
                if club1_id == club2_id:
                    error += 'Fehler: Es müssen zwei unterschiedliche Vereine zum Zusammenlegen ausgewählt werden!' + '<br />'
                error += 'Fehler: Zusammenlegen von Vereinen noch nicht implementiert!' + '<br />'
                pass
    # Ausgabe
    content = {
        'club_list':club_query, 'merge_to_club_persons': merge_to_club_persons, 'club_to_merge_persons':club_to_merge_persons, 'club1_id':club1_id, 'club2_id':club2_id, 'text':text, 'error':error, 'response':response, 'event':event,
    }
    return render(request, 'vunireg/entrys_club_merge.html', content)
    
# Funktion, die das Updaten der Teams ermöglicht
### TODO ###
### Ggf. nur für Meldungen, die noch keinem Team Zugeordnet sind und nicht für alle Meldungen
### ENDTODO ###
def entrys_update_teams(event_id):
    event = get_object_or_404(Event, pk=event_id)
    #### T0DO ###
    # Dynamisch übergeben des Dateforage
    dateforage = event.startdate
    ### ENDTODO ### 
    response = ''
    error = ''
    # Alle Team-Disziplinen der Veranstaltung ermitteln
    team_discipline_query = Discipline.objects.filter(competition__in=Competition.objects.filter(event__id=event.pk, discipline__isgroupdisc__gt=1)).distinct()
    print(team_discipline_query)
    # Alle Einzel-Meldungen zu diesen Disziplinen ermitteln
    entrys_for_teams_query = Entry.objects.filter(event__id=event.pk, discipline__in=team_discipline_query, person__isnull=False)
    print(entrys_for_teams_query)
    # Alle Meldungen durchgehen und schauen, ob die Team-Zuordnung passt
    for entry in entrys_for_teams_query:
        # Teamnamen aus der Meldung ermitteln
        data = entry.value.split(';')
        if len(data) == 1:
            print('Zu wenig Argumente Übergeben')
        elif len(data) == 2 and data[0] != '':
            team_from_entry = data[0]
            # Wenn es bereits ein passendes Team gibt, Meldung dem Team zuteilen, sonst Team erstellen
            try:
                entry_team = Team.objects.get(name__iexact=team_from_entry, discipline=entry.discipline)
                print('Team ' + entry_team.name + ' existiert bereits.')
                entry.entry_team = entry_team
                entry.save()
                ### TODO ###
                ### Ggf. muss das "Alter" der Staffel an den neuen Teilnehmer angepasst werden
                ### Implementierung vielleicht wo anders sinnvoller? Es können auch Mitglieder aus dem Team entfernt werden, wenn es zu viele sind!
                if entry.person.age_at(dateforage) > entry_team.ageforcomp:
                    entry_team.ageforcomp = entry.person.age_at(dateforage)
                    entry_team.save()
                    print('Team hat jetzt das neue Maximale Alter ' + str(entry_team.ageforcomp) )
                ### ENDTODO ###
            except Team.DoesNotExist:
                print('Team ' + team_from_entry + ' existiert noch nicht.')
                entry_team = Team(name=team_from_entry, discipline=entry.discipline, ageforcomp=entry.person.age_at(dateforage), event=event)
                entry_team.save()
                entry.entry_team = entry_team
                entry.save()
        else:
            print('Zu viele Argumente oder keinen gültigen Team-Namen übergeben')
    return