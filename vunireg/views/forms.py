from django import forms
from django.forms.models import inlineformset_factory, modelformset_factory

# Import Models from models.py
from vunireg.models import Event, Discipline, Competition, Person, Team, Club, Entry, Scoringcategory, Qualification, Start, Result

# Create Forms

# Form zum Bearbeiten der Event Daten
class EventEditForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
        widgets = {
            'description': forms.Textarea(attrs={'cols':25, 'rows':10,}),
        }

# Form zum Bearbeiten der Personen Daten
class PersonEditForm(forms.ModelForm):
    club = forms.ModelChoiceField(queryset = Club.objects.all(), empty_label="")
    class Meta:
        model = Person
        exclude = ('startnr',),

# BaseForm zum Bearbeiten der Meldungen, nimmt event_id als Argument entgegen und filtert das Queryset entsprechend
class BaseEntryEditFormSet(forms.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        self.event_id = kwargs.pop('event_id', None)
#        self.counter = 0
        super(BaseEntryEditFormSet, self).__init__(*args, **kwargs)
 
    def get_queryset(self):
#        self.counter = self.counter + 1
#        print('Filtern des Queryset ' + str(self.counter) )
        return forms.BaseInlineFormSet.get_queryset(self).filter(event__id = self.event_id)

# Inline-Form zum Bearbeiten der Meldungen
EntryEditInlineForm = inlineformset_factory(
    Person,
    Entry,
    formset = BaseEntryEditFormSet,
    exclude = ('event', 'team', 'competition',),
    extra=1,
)

class TeamEditForm(forms.ModelForm):
    ### TODO ###
    ### Begrenzen der Auswahl auf die bei dem Event vorhandenen Disziplinen
    ### competition__in=Competition.objects.filter(event__id=event.pk).distinct() --> event?
    discipline = forms.ModelChoiceField(queryset=Discipline.objects.filter(isgroupdisc__gt=1))
    ### ENDTODO ###
    class Meta:
        model = Team
        exclude = ('event', 'ageforcomp', )

class ClubEditForm(forms.ModelForm):
    class Meta:
        model = Club
        fields = '__all__'
        
#    def clean(self):
#        if str(self.cleaned_data['time']) == "-----------":
#            raise forms.ValidationError('Please choose time.')
#        if self.cleaned_data['dailynote'] == "":
#            raise forms.ValidationError('Please enter note.')
#        if not self.cleaned_data['teacher']:
#            raise forms.ValidationError('Please choose teacher .')


class ReadOnlyForm(EventEditForm):
    def __init__(self, *args, **kwargs):
        super(EventEditForm, self).__init__(*args, **kwargs)
        for key in self.fields.keys():
            self.fields[key].widget.attrs['readonly'] = True
            
class ScoringcatEditForm(forms.ModelForm):
    class Meta:
        model = Scoringcategory
        exclude = ('event', )
        
class QualificationEditForm(forms.ModelForm):
    class Meta:
        model = Qualification
        exclude = ('event', )
        
class ResultEditForm(forms.ModelForm):
    class Meta:
        model = Result
        exclude = ('start', 'judge')
        
ResultEditInlineFormset = modelformset_factory(
    Result,
    extra = 0,
    exclude = ('event', 'person', 'discipline', 'start', 'judge', 'diff', 'custom2', 'timestamp', ),
    widgets = {
            'value': forms.TextInput(attrs={'placeholder': 'Leistung'}),
            'custom1': forms.TextInput(attrs={'placeholder': 'Wind'}),
            'custom2': forms.TextInput(attrs={'placeholder': 'Status'}),
            'comment': forms.TextInput(attrs={'placeholder': 'Kommentar'}),
        }
)
