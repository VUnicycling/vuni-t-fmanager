from django.db import models



DISCIPLINEGROUP_CHOICES = (
    ('', ''),
    ("TRA", "Track & Field"),
    ("FRE", "Freestyle"),
    ("MUN", "Muni"),
    ("ROA", "Road"),
    ("URB", "Urban"),
    ("TEA", "Team"),
    ("CON", "Convention"),
    ("OTH", "Other"),
)

RANK_CHOICES = (
	('ASC', 'Aufsteigend'),
    ('DSC', 'Absteigend'),
)

SCORING_CHOICES = (
    ('COM', 'Basis: Wettbewerb (AK)'),
    ('DIS', 'Basis: Disziplin (AK)'),
    ('EVT', 'Basis: Wettkampf (AK)'),
    ('FIN', 'Basis: Wettbewerb (FINAL)'),
)

SCORINGFILTER_CHOICES = (
    ('club', 'Verein des Teilnehmers'),
    ('custom1', 'Staatsangeh. (custom1) des Teilnehmers'),
)

GENDER_CHOICES = (
    ('', ''),
    ('f', 'weiblich'),
    ('m', 'männlich'),
)

ENTRYTYPE_CHOICES = (
    ('', ''),
    ('NM', 'NM'),
    ('AK', 'AK'),
)


class Discipline(models.Model):
    name = models.CharField("Bezeichnung", max_length=100)
    code = models.CharField("Code/Kurzform", max_length=5, unique=True)
    group = models.CharField("Zugehörigkeit der Disziplin", max_length=3, choices=DISCIPLINEGROUP_CHOICES)
    ranking = models.CharField("Sortierung für Rangfolge", max_length=3, choices=RANK_CHOICES)
    mixedgender = models.BooleanField("Keine Geschlechtertrennung", default=False)
    # Für die Festlegung, ob in der Disziplin männliche und weibliche Fahrer gemischt werden dürfen (z.B. Staffel)
    isgroupdisc = models.PositiveIntegerField("Maximale Teilnehmeranzahl pro Start", default=1)
    # Das isgroupdisc Feld legt fest, wie viele Teilnehmer maximal bei einer Gruppendisziplin in einer Gruppe seien dürfen
    #	1. Einzeldisziplin (Wert '1' oder None)
    #	2. Staffeldisziplin (Wert '4')
    custom1 = models.CharField("CustomField1", max_length=100, null=True, blank=True)
    # Das CustomField1 wird bei den Track&Field Disziplinen genutzt um zu hinterlegen, ob die Starts
    #	1. Auf Sprintbahnen erfolgen (Wert 'ILS')
    #	2. Auf Rundbahnen erfolgen (Wert 'ILR')
    #	3. In offener Startreihenfolge erfolgen (Wert 'FRE')
    custom2 = models.CharField("CustomField2", max_length=100, null=True, blank=True)
    # Das CustomField2 wird bei den Track&Field Disziplinen genutzt um zu hinterlegen, ob das Finale standardmäßig ausgefahren wird
    # und ob ggf. Einschränkungen bei der Ermittlung der Finalisten bestehen (z.B. 10m Radlauf)
    #	1. Ja, separater Finallauf ohne Einschränkungen (Wert 'FINAL')
    #	2. Ja, separater Finallauf mit Einschränkungen (Wert 'PARTFINAL')
    #	3. Nein, Finalwertung aus AK Ergebnissen ohne Einschränkungen (Wert 'AGFINAL')
    #	4. Nein, Finalwertung aus AK Ergebnissen mit Einschränkungen (Wert 'PARTAGFINAL')
    
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name = "Disziplin"
        verbose_name_plural = "Disziplinen"
        ordering = ("pk", "pk")
        
        
        
class Agegroup(models.Model):
    name = models.CharField("Bezeichnung", max_length=100)
    code = models.CharField("Code/Kurzform", max_length=3, unique=True)
    group = models.CharField("Zugehörigkeit der Altersklasse", max_length=3, choices=DISCIPLINEGROUP_CHOICES)
    maxage = models.PositiveIntegerField("Alter bis einschließlich")
    
    def __str__(self):
        return 'AK ' + (self.name)
        
    class Meta:
        verbose_name = "Altersklasse"
        verbose_name_plural = "Altersklassen"
        ordering = ("group", "name", "maxage")



class Event(models.Model):
    name = models.CharField("Vollständiger Name", max_length=100)
    slug = models.SlugField("Kürzel", help_text="Kürzel für die URL: nur Buchstaben, Zahlen und Striche - keine Leerzeichen!", unique=True)
    startdate = models.DateField("Von")
    enddate = models.DateField("Bis")
    description = models.TextField("Beschreibung")
    group = models.CharField("Wettkampftyp", max_length=3, choices=DISCIPLINEGROUP_CHOICES)
    location = models.CharField("Wettkampfort", max_length=100)
    lanesstr = models.PositiveIntegerField("Sprintbahnen", default=8)
    lanesrnd = models.PositiveIntegerField("Rundbahnen", default=8)

    host = models.CharField("Ausrichter", max_length=100)
    contact = models.CharField("Ansprechpartner", max_length=100)
    contactmail = models.EmailField("E-Mail Adresse")
    
    def __str__(self):
        return self.name
        
    class Meta:
        verbose_name = "Veranstaltung"
        verbose_name_plural = "Veranstaltungen"
        ordering = ("startdate", "name")



class Competition(models.Model):
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    fullname = models.CharField('Bezeichnung', max_length=50)
    discipline = models.ForeignKey(Discipline, on_delete=models.PROTECT)
    round = models.PositiveIntegerField('Runde (AK=1;FINAL=2)', default=1)
    scoringcat = models.ForeignKey('Scoringcategory', on_delete=models.PROTECT, null=True, blank=True)
    maxage = models.PositiveIntegerField('Alter bis einschließlich', default=100)
    gender = models.CharField('Geschlecht', max_length=1, choices=GENDER_CHOICES)
    ismerged = models.BooleanField('Wettbewerb zusammengelegt', default=False)
    entrycount = models.PositiveIntegerField('Anzahl Meldungen', default=0)
    
    def __str__(self):
        return (self.discipline.name) + ' ' + (self.fullname)
        
    class Meta:
        verbose_name = 'Wettkampf'
        verbose_name_plural = 'Wettkämpfe'
        ordering = ('maxage', 'gender', 'round')
        
        
        
class Scoringcategory(models.Model):
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    fullname = models.CharField("Bezeichnung", max_length=50)
    shortdescription = models.CharField("Kurzbezeichnung z.B. für die Bezeichung in den Ergebnislisten (Finale; Juniorfinale)", max_length=20, blank=True)
    description = models.TextField("Beschreibung")
    type = models.CharField("Wertungsart", max_length=3, choices=SCORING_CHOICES)
    filtertype1 = models.CharField("Filtertyp 1", max_length=10, choices=SCORINGFILTER_CHOICES, null=True, blank=True)
    filtervalue1 = models.CharField("Filterwert 1", max_length=50, null=True, blank=True)
    filtertype2 = models.CharField("Filtertyp 2", max_length=50, choices=SCORINGFILTER_CHOICES, null=True, blank=True)
    filtervalue2 = models.CharField("Filterwert 2", max_length=50, null=True, blank=True)
    showexternal = models.BooleanField("Ergebnisliste mit Fahrern außer Konkurenz", default=True)
    minage = models.PositiveIntegerField("Alter von", default=0)
    maxage = models.PositiveIntegerField("Alter bis", default=100)
    usequali = models.BooleanField('Mindestqualifikation anwenden', default=True)
    
    def __str__(self):
        return self.fullname
        
    class Meta:
        verbose_name = "Wertungskategorie"
        verbose_name_plural = "Wertungskategorien"
        ordering = ("fullname", "type")



class Qualification(models.Model):
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    discipline = models.ForeignKey(Discipline, on_delete=models.PROTECT)
    minage = models.PositiveIntegerField('Alter von', default=0)
    maxage = models.PositiveIntegerField('Alter bis', default=100)
    value = models.CharField('Mindestqualifikation', max_length=100, null=True, blank=True)
    
    def __str__(self):
        return str(self.discipline) + ' U' + str(self.maxage + 1)
        
    class Meta:
        verbose_name = 'Mindestqualifikation'
        verbose_name_plural = 'Mindestqualifikationen'
        ordering = ('discipline', 'maxage')


class Person(models.Model):
    forename = models.CharField('Vorname', max_length=100)
    surname = models.CharField('Nachname', max_length=100)
    gender = models.CharField('Geschlecht', max_length=1, choices=GENDER_CHOICES)
    dateofbirth = models.DateField()
    otheragegroup = models.BooleanField('Abweichender Start in U13', default=False)
    # Das Feld otheragegroup dient dazu U11 Fahrer zu markieren, die abweichend mit einem 24" Rad in der U13 starten
    club = models.ForeignKey('Club', null=True, on_delete=models.SET_NULL)
    iscontact = models.BooleanField('Ist Kontaktperson', default=False)
    custom1 = models.CharField('CustomField1', max_length=100, null=True, blank=True)
    # Das CustomField1 kann dazu genutzt werden z.B. die Staats-/Landes-/Regionsangehörigkeit zu speichern
    # Darüber lässt sich die automatische Zuordnung zu verschiedenen Wertungen (z.B. "International/Offen", "Land", "Region") realisieren
    custom2 = models.CharField('CustomField2', max_length=100, null=True, blank=True)
    startnr = models.PositiveIntegerField('Startnummer', null=True, blank=True, unique=True, default=None)
    
    def __str__(self):
        return (self.forename) + ' ' + (self.surname)
    
    def age_at(self, dateforage):
        age = dateforage.year - self.dateofbirth.year - ((dateforage.month, dateforage.day) < (self.dateofbirth.month, self.dateofbirth.day))
        return age

    class Meta:
        verbose_name = 'Teilnehmer'
        verbose_name_plural = 'Teilnehmer'
        ordering = ('club', 'surname')
        constraints = [
            models.UniqueConstraint(fields=['forename', 'surname', 'dateofbirth'], name='unique_person')
        ]



class PersonDetails(models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE, primary_key=True)
    mail = models.EmailField("E-Mail Adresse")
    phone1 = models.CharField("1. Telefonnummer", max_length=20)
    phone2 = models.CharField("2. Telefonnummer", max_length=20)
    adress = models.CharField("Straße", max_length=100)
    zipcode = models.CharField("PLZ", max_length=20)
    city = models.CharField("Ort", max_length=100)
    state = models.CharField("Land", max_length=100)
    
    def __str__(self):
        return self.person
        
    class Meta:
        verbose_name = "Persönliche Daten"
        verbose_name_plural = "Persönliche Daten"
        ordering = ("person", "person")
        


class Club(models.Model):
    clubname = models.CharField(max_length=100)
    individualstart = models.BooleanField("Einzelstarter", null=True, default=False)
    contactperson = models.OneToOneField(Person, related_name='contact', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.clubname
        
    class Meta:
        verbose_name = "Verein"
        verbose_name_plural = "Vereine"
        ordering = ("clubname", "clubname")



class Team(models.Model):
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    name = models.CharField('Teamname', max_length=100)
    ageforcomp = models.PositiveIntegerField('Maximales Alter', null=True, blank=True, default=0)
    # Die Disziplin ist redundant! eigentlich reicht der Link zum Entry? ABER: Wie Unique sicherstellen?
    discipline = models.ForeignKey(Discipline, on_delete=models.PROTECT)
    
    def __str__(self):
        return str(self.discipline) + ' Team: ' + str(self.name)
        
    class Meta:
        verbose_name = 'Team'
        verbose_name_plural = 'Teams'
        ordering = ('discipline', 'name')
        constraints = [
            models.UniqueConstraint(fields=['name', 'discipline'], name='unique_team')
        ]


class Entry(models.Model):
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    person = models.ForeignKey(Person, on_delete=models.PROTECT, null=True, blank=True)
    team = models.OneToOneField(Team, related_name='entry', on_delete=models.CASCADE, null=True, blank=True)
    discipline = models.ForeignKey(Discipline, on_delete=models.PROTECT)
    # Sofern eine Einzel-Meldung zu einer Team-Disziplin gehört, wird das entsprechende Team gespeichert, ansonsten bleibt das Feld leer
    entry_team = models.ForeignKey(Team, related_name='team_entries', on_delete=models.SET_NULL, null=True, blank=True)
    competition = models.ForeignKey(Competition, on_delete=models.PROTECT, null=True, blank=True)
    value = models.CharField(max_length=100, null=True, blank=True)
    
    def __str__(self):
        if self.person != None:
            return str(self.person) + ' ' + str(self.discipline)
        else:
            return str(self.team) + ' ' + str(self.discipline)
        
    class Meta:
        verbose_name = "Meldung"
        verbose_name_plural = "Meldungen"
        ordering = ("person", "discipline")
        constraints = [
            models.UniqueConstraint(fields=["event", "person", "discipline"], name='unique_entry')
        ]



class Start(models.Model):
    ### TODO ###
    ### Überprüfen, ob die Verknüpfung wirklich notwendig ist - lässt sich für jeden Start feststellen auch über die Meldung herausfinden
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    person = models.ForeignKey(Person, on_delete=models.PROTECT, null=True, blank=True)
    competition = models.ForeignKey(Competition, on_delete=models.PROTECT)
    ### ENDTODO ###
    entry = models.ForeignKey(Entry, on_delete=models.PROTECT, null=True, blank=True)
    heat = models.PositiveIntegerField('Lauf', default=0, null=True, blank=True)
    lane = models.PositiveIntegerField('Bahn', default=0, null=True, blank=True)
    value = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        if self.entry == None:
            return str(self.person)
        elif self.entry.person != None:
            return str(self.entry.person) + ' ' + str(self.competition)
        else:
            return str(self.entry.team) + ' ' + str(self.competition)
        
    class Meta:
        verbose_name = 'Start'
        verbose_name_plural = 'Starts'
        ordering = ('competition', 'person')


  
class Result(models.Model):
    ### TODO ###
    # Überprüfen, an welchen Stellen die folgenden Informationen überhaupt sinnvoll sind und benötigt werden
    event = models.ForeignKey(Event, on_delete=models.PROTECT, null=True, blank=True)
    person = models.ForeignKey(Person, on_delete=models.PROTECT, null=True, blank=True)
    discipline = models.ForeignKey(Discipline, on_delete=models.PROTECT, null=True, blank=True)
    ### ENDTODO ###
    start = models.ForeignKey(Start, on_delete=models.PROTECT, null=True, blank=True)
    judge = models.CharField('Judge', max_length=100, null=True, blank=True)
    value = models.CharField('Leistung', max_length=100, null=True, blank=True)
    diff = models.CharField('Differenz', max_length=100, null=True, blank=True)
    custom1 = models.CharField('CustomField1', max_length=100, null=True, blank=True)
    # Das CustomField1 wird bei den Track Disziplinen dazu verwendet den Wind zu speichern
    custom2 = models.CharField('CustomField2', max_length=100, null=True, blank=True)
    # Das CustomField2 wird bei den Track&Field Disziplinen dazu verwendet den Status des Ergebnisses zu specihern
    # (z.B. ungültig Ergebinsse)
    comment = models.CharField('Kommentar', max_length=100, null=True, blank=True)
    timestamp = models.DateTimeField('Zeitstempel', null=True, blank=True)
    
    def __str__(self):
        return str(self.start.person) + str(self.start.competition.discipline)
        
    class Meta:
        verbose_name = 'Ergebnis'
        verbose_name_plural = 'Ergebnisse'
        ordering = ('start', 'value')