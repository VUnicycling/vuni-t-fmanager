from django.contrib import admin

# Register your models here.

from vunireg.models import Event, Person, Team, Club, Entry, Discipline, Agegroup, Competition, Scoringcategory, Start, Result


class EventAdmin(admin.ModelAdmin):
	fieldsets = [
		(None,				{'fields':['name', ('startdate', 'enddate'), 'location', 'description']}),
		('Kontakt',		{'fields':['host', 'contact', 'contactmail']}),
	]
	list_display = ('name', 'startdate', 'enddate', 'location', 'host')


class ClubAdmin(admin.ModelAdmin):
	list_display = ('clubname', 'individualstart', 'contactperson')


class EntryInline(admin.TabularInline):
	model = Entry
	extra = 1
	
class PersonAdmin(admin.ModelAdmin):
	inlines = [EntryInline]
	list_display = ('forename', 'surname', 'gender', 'dateofbirth', 'club', 'custom1')


class EntryAdmin(admin.ModelAdmin):
	list_display = ('person', 'discipline', 'competition', 'value', 'team')


class StartAdmin(admin.ModelAdmin):
	list_display = ('person', 'entry', 'competition', 'heat', 'lane', 'value', 'event')


class ResultAdmin(admin.ModelAdmin):
	list_display = ('person', 'discipline', 'value', 'diff', 'custom1', 'custom2', 'comment', 'timestamp')


admin.site.register(Event, EventAdmin)
admin.site.register(Club, ClubAdmin)
admin.site.register(Entry, EntryAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Team)
admin.site.register(Discipline)
admin.site.register(Agegroup)
admin.site.register(Competition)
admin.site.register(Start, StartAdmin)
admin.site.register(Scoringcategory)
admin.site.register(Result, ResultAdmin)
